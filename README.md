Social Datamining Tool

This tool allows to generate various statistics and general feelings by using Facebook, Twitter and other sources.

### How do I get set up? ###

* Fetch the source code using Git
* Fetch all dependencies using Maven
* Start programming

### Contribution guidelines ###

* Write tests
* Do not push broken/uncompilable code

### 3rd Party Licenses ###

SDM-Task2-Tool uses JxBrowser http://www.teamdev.com/jxbrowser, which is a proprietary software. The use of JxBrowser is governed by JxBrowser Product Licence Agreement http://www.teamdev.com/jxbrowser-licence-agreement. If you would like to use JxBrowser in your development, please contact TeamDev.