/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool;

import java.awt.Dimension;

import com.robertkoszewski.sdu.sdm.sdmtool.browser.BrowserFactory.DefaultBrowser;

public class Configuration {
	
	// App Name
	public static final String 		app_name 				= "Social Datamining Tool";
	// App ID
	public static final String		app_id					= "sdmtool";
	// App Version
	public static final String 		app_version 			= "1.0.0.0";
	// App Icon
	public static final String		app_icon 				= "/web/media/icon.png";
	// Default Window Size
	public static final Dimension 	app_window_size 		= new Dimension(740, 500);
	public static final boolean 	app_window_maximized 	= true;
	// Console Name
	public static final String 		console_prompt 			= "SDM-Tool";
	
	// Settings
	public static DefaultBrowser browser_default 		= DefaultBrowser.JxBrowserSwing;

	// FXBrowser Configuration
	public static final String 		fxbrowser_bin_dir 		= null; // If NULL then extraction will be to default TEMP folder
	public static final boolean 	fxbrowser_show_license_message = false; // This doesn't seem to work when Evaluation license is used
	
	// WUI Confiration
	public static final String		wui_template 			= "gentelella";
	
}
