/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool;

import java.io.File;
import java.io.IOException;

import com.robertkoszewski.sdu.sdm.sdmtool.browser.BrowserFactory;
import com.robertkoszewski.sdu.sdm.sdmtool.lingtools.LingTools;
import com.robertkoszewski.sdu.sdm.sdmtool.lingtools.LinguisticPolarity;

import asg.cliche.Command;
import asg.cliche.Param;

public class ConsoleShell { // More info at: http://cliche.sourceforge.net/?f=manual	
	
	@Command(description="Stops the server")
	public void stop() {
		spark.Spark.stop(); // Stop Web Server
	    System.exit(0); // Stop App
	}
	
	@Command(description="Opens the APP GUI") // Show APP UI
	public void opengui() throws InterruptedException {
		BrowserFactory.getBrowser().openBrowserWindow("http://www.gooogle.es", Configuration.app_name, Configuration.app_window_size, Configuration.app_window_maximized, Configuration.app_icon, null);
	}
	
	@Command (description="Opens a browser window with the selected URL") // Show Browser UI
	public void openbrowser(@Param(name="url", description="URL to be opened in the browser") String url) throws InterruptedException {
		BrowserFactory.getBrowser().openBrowserWindow(url, Configuration.app_name+" - Browser: "+url, Configuration.app_window_size, Configuration.app_window_maximized, Configuration.app_icon, null);
	}

	@Command (description="Load a Trained Linguistic Model") // Load Linguistic Model
	public void lingLoad(String filename){
		System.out.println("Loading Model File: "+filename);
		LinguisticPolarity ling = LingTools.getIntance();
		
		String working_dir;
		if(System.getProperty("user.home") != null && !System.getProperty("user.home").equals(""))
        	working_dir = System.getProperty("user.home") + File.separatorChar + "." + Configuration.app_id + File.separatorChar;
        else
        	working_dir = "."+File.separatorChar;
		
        File model = new File(working_dir + filename + ".model");

		try {
			ling.loadModel(model);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("SUCCESS");
	}
	
	@Command (description="Train Linguistic Model") // Load Linguistic Model
	public void lingTrain(){
		LinguisticPolarity ling = LingTools.getIntance();
		
		String working_dir;
		if(System.getProperty("user.home") != null && !System.getProperty("user.home").equals(""))
        	working_dir = System.getProperty("user.home") + File.separatorChar + "." + Configuration.app_id + File.separatorChar;
        else
        	working_dir = "."+File.separatorChar;
		
		ling.train(new File(working_dir+"trainingset")); // Start Training
		
		System.out.println("Training finished. Don't forget to save the model using 'ling-save <modelname>'");
	}
	
	@Command (description="Test Current Ling Model")
	public void lingTest(String text){
		LinguisticPolarity ling = LingTools.getIntance();
		if(!ling.isTrained()){
			System.out.println("ERROR: Linguistic model is not trained. Load a Model or Train it first.");
		}else{
			//System.out.println(ling.clasify(text));
			switch(ling.clasify(text)){
			case "pos":
				System.out.println("POSITIVE");
				break;
			case "neg":
				System.out.println("NEGATIVE");
				break;
			default:
					System.out.println("UNKNOWN CATEGORY");
			}
		}
	}
	
	@Command (description="Save Current Ling Model")
	public void lingSave(String model){
		LinguisticPolarity ling = LingTools.getIntance();
		
		String working_dir;
		if(System.getProperty("user.home") != null && !System.getProperty("user.home").equals(""))
        	working_dir = System.getProperty("user.home") + File.separatorChar + "." + Configuration.app_id + File.separatorChar;
        else
        	working_dir = "."+File.separatorChar;
		
		System.out.println("Saving model...");
		try {
			ling.saveModel(new File(working_dir + model +".model"));
			System.out.println("DONE.");
		} catch (Exception e) {
			System.out.println("ERROR SAVING MODEL.");
			e.printStackTrace();
		}
		
	}
	
}
