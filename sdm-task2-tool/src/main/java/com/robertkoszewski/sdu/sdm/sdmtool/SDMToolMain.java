/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool;

import static spark.Spark.*;
import java.io.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.robertkoszewski.sdu.sdm.sdmtool.browser.BrowserFactory;
import com.robertkoszewski.sdu.sdm.sdmtool.browser.BrowserFactory.DefaultBrowser;
import com.robertkoszewski.wui.*;
import com.robertkoszewski.wui.templates.GentelellaTemplate;

import asg.cliche.ShellFactory;

/**
 * Social Datamining Tool Main Class
 * @author Robert Koszewski
 */
public class SDMToolMain {

	/**
	 * Starting Point
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		// App Splash
		System.out.println(Configuration.app_name + " - Version: "+Configuration.app_version);
		
		// Parse args
		boolean headless_mode = false;
		int forced_port = 0;
		
		for(String arg: args){
			switch(arg){
			case "headless":
			case "server":
				headless_mode = true; break;
				
			case "jxbrowserjavafx":
				Configuration.browser_default = DefaultBrowser.JxBrowserJavaFX; break;
				
			case "jxbrowser":
			case "jxbrowserswing":
				Configuration.browser_default = DefaultBrowser.JxBrowserSwing; break;
				
			case "javafxwebview":
				Configuration.browser_default = DefaultBrowser.JavaFXWebView; break;
				
			case "native":
				Configuration.browser_default = DefaultBrowser.NativeBrowser; break;
				
			default:
				if(arg.startsWith("port:")){
					try{
						forced_port = Integer.parseInt(arg.substring("port:".length()));
					}catch(Exception e){
						System.err.println("Error: Could not set port number.");
						e.printStackTrace();
					}
				}

			}
		}
		
		// Set Logger Verbosity
		// TODO: -log:(debug,verbose,error)
		System.setProperty(org.slf4j.impl.SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "ERROR");
		
		// Initialize Template
		WindowTemplate template = new GentelellaTemplate();
		template.setAppName("Social Datamining Tool");
		
		// Initialize Content Manager
		ContentManager content_manager = new ContentManager(template, Configuration.app_id, forced_port);

		// OnClose Operation
		Runnable onCloseOperation = new Runnable(){

			@Override
			public void run() {
				stop();
				System.exit(0); // Just Close Everything
			}
			
		};

		// Server Initialized Message
		Logger logger = LoggerFactory.getLogger(SDMToolMain.class);
	    logger.info("Server started on port: "+content_manager.getServerPort());
	    
	    // Static Console Info
	    System.out.println("Server started on port: "+content_manager.getServerPort());
	    
	    // Open Browser Window
	    // TODO: If -dediserver flag is set then do not open the BrowserWindow
	    if(!headless_mode){
	    	BrowserFactory.getBrowser().openBrowserWindow("http://localhost:"+content_manager.getServerPort(), Configuration.app_name, Configuration.app_window_size, Configuration.app_window_maximized, Configuration.app_icon, onCloseOperation);
	    }
	    
	    // Start Shell
	    ShellFactory.createConsoleShell(Configuration.console_prompt, "Welcome to the "+Configuration.app_name+" shell. "
	    		+ "For a list of commands just type '?list' into the command shell or '?help <command>' for more detailed "
	    		+ "information about each command.", new ConsoleShell()).commandLoop(); // and three.
		
	}
}
