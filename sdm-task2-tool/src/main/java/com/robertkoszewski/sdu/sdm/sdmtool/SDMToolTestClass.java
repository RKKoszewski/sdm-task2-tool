/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool;

import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.SocialProvider;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.UserPostData;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.twitter.TwitterProvider;

import java.util.List;

/**
 * Quick Test Class for Provider Classes
 * @author Robert Koszewski
 *
 */
public class SDMToolTestClass {

	public static void main(String[] args) {
		System.out.println("SDM Tool Test Class:");
		/*
		// Query
		FacebookQuery fq = new FacebookQuery();
		fq.search_string = "#brangelina";
		
		// Test Provider Class
		SocialProvider fp = new FacebookProvider();
		fp.ProviderInit();
		*/

		//TwitterQuery tq = new TwitterQuery();
		//tq.search_string = "Trump";

		SocialProvider tp = new TwitterProvider();
		tp.ProviderInit(null);
		tp.setQuery("Trump");

		// Fetch Data
		List<UserPostData> d = null;
		try {
			d = tp.getUserPosts();
		} catch (Exception e) {
			e.printStackTrace();
		}
				
		// Print Results
		if(d == null)
			System.err.println("ERROR: No dataset returned");
		else
			for(UserPostData su: d)
				System.out.println("["+su.timestamp+"] "+su.userName+" ("+su.userID+") - "+su.commentData);

	}

}
