/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.browser;

import com.robertkoszewski.sdu.sdm.sdmtool.Configuration;

/**
 * Generates a Browser Instance
 * @author Robert Koszewski
 */
public class BrowserFactory {

	private static Browser browser_instance = null;
	
	/**
	 * Get Browser Instance
	 * @return Browser
	 */
	public synchronized static Browser getBrowser(){
		if(browser_instance == null) initBrowser();
		return browser_instance;
	}
	
	/**
	 * Initializes Browser
	 */
	private static void initBrowser(){
		switch(Configuration.browser_default){
		case JxBrowserJavaFX:
			browser_instance = new JxBrowserJavaFXBrowser();
			break;
		case JxBrowserSwing:
			browser_instance = new JxBrowserSwingBrowser();
			break;
		case JavaFXWebView:
			browser_instance = new JavaFXWebViewBrowser();
			break;
		default:
			browser_instance = new NativeBrowser();
			break;
		
		}
	}
	
	/**
	 * Default Browsers
	 */
	public enum DefaultBrowser{
		JavaFXWebView,
		JxBrowserSwing,
		JxBrowserJavaFX,
		NativeBrowser
	}
}
