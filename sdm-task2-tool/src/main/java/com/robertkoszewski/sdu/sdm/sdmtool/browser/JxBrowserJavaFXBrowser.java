/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.browser;

import java.awt.Dimension;

import com.robertkoszewski.sdu.sdm.sdmtool.Configuration;
import com.robertkoszewski.utils.Utils;
import com.robertkoszewski.wui.browsers.javafx.JavaFXSubsystem;
import com.teamdev.jxbrowser.chromium.BrowserCore;
import com.teamdev.jxbrowser.chromium.internal.Environment;
import com.teamdev.jxbrowser.chromium.javafx.BrowserView;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.*;

public class JxBrowserJavaFXBrowser implements Browser{
	public void openBrowserWindow(String url, String title, Dimension window_size, boolean window_maximized, String icon_path, Runnable onCloseAction) {
		// Start JavaFX Subsystem
		JavaFXSubsystem.initialize();
		
		// Start JavaFX WebView Browser
		Platform.runLater(new Runnable() {
		    @Override
		    public void run() {
		    	
		    	JxBrowserLicense.isLicensed();
		    	
		    	// Start JavaFX Window
		    	final Stage stage = new Stage(); // Initialize Stage
		    	stage.setTitle(title); // Set Title
		        if(icon_path!=null) stage.getIcons().add(Utils.getFXImage(icon_path)); // Set Icon	
		        
		        // ## Use FXBrowser ##
	        	if(Configuration.fxbrowser_bin_dir != null) // Set Binary Extraction Dir
	        		System.setProperty("jxbrowser.chromium.dir", Configuration.fxbrowser_bin_dir);
	        	
	        	// Show/Hide License Message
	        	System.setProperty("teamdev.license.info", (Configuration.fxbrowser_show_license_message?"true":"false"));
	        	
	        	if (Environment.isMac()) { // Initialize Browser on MAC
	                BrowserCore.initialize();
	            }
	        	
	        	com.teamdev.jxbrowser.chromium.Browser browser = new com.teamdev.jxbrowser.chromium.Browser();
	            BrowserView view = new BrowserView(browser);
	            browser.loadURL(url);
	            
	            // Set Window Size
	            if(window_size==null)
	            	stage.setScene(new Scene(new BorderPane(view), Configuration.app_window_size.getWidth(), Configuration.app_window_size.getHeight()));
	            else
	            	stage.setScene(new Scene(new BorderPane(view), window_size.getWidth(), window_size.getHeight()));
		      
	            // Set Maximized State
	    		if(window_maximized)
	    			stage.setMaximized(true);
		       
		    	// Set On Close Action
		        if(onCloseAction!=null){ // Don't add if not used
			        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			            public void handle(WindowEvent we) {
			            	onCloseAction.run();
			            }
			        });   
		        }
		        
		        // Show Window
		    	stage.show();
		    	stage.toFront();
		    }
		});
		
	}
}
