/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.browser;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;
import javax.swing.JFrame;

import com.robertkoszewski.sdu.sdm.sdmtool.Configuration;
import com.robertkoszewski.utils.Utils;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;

/**
 * JavaFX FXBrowser
 * @author Robert Koszewski
 */
public class JxBrowserSwingBrowser implements Browser{

	public void openBrowserWindow(String url, String title, Dimension window_size,  boolean window_maximized, String icon_path, Runnable onCloseAction) {
		
		JxBrowserLicense.isLicensed();
		
		// Use Java Swing with FXBrowser
		
		// Show/Hide License Message
    	System.setProperty("teamdev.license.info", (Configuration.fxbrowser_show_license_message?"true":"false"));
		
    	// Build Browser
		com.teamdev.jxbrowser.chromium.Browser browser = new com.teamdev.jxbrowser.chromium.Browser();
		BrowserView view = new BrowserView(browser);
		 
		// Show Java Swing JFrame
		JFrame frame = new JFrame();
		frame.setTitle(title);
		if(onCloseAction!=null){ // Don't add if not used
			frame.addWindowListener(new java.awt.event.WindowAdapter() {
			    @Override
			    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
			    	onCloseAction.run();
			    }
			});
		}
		
		if(icon_path!=null){
			try {
				frame.setIconImages(Utils.getSwingImage(icon_path));
			} catch (IOException e) {
				e.printStackTrace();
			} // Set Icon	
		}
		
		frame.add(view, BorderLayout.CENTER);

		// Set Window Size
		if(window_size!=null){
			frame.setSize(window_size);
		}else{
			frame.setSize(Configuration.app_window_size);
		}
		
		// Set Maximized State
		if(window_maximized)
			frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
		
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		browser.loadURL(url);
		
	}

}
