/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.controllers;

import com.robertkoszewski.sdu.sdm.sdmtool.views.DashboardView;
import com.robertkoszewski.utils.SystemInfo;
import com.robertkoszewski.wui.*;
import com.robertkoszewski.wui.widgets.*;
import spark.Request;
import spark.Response;

/**
 * Dashboard Controller
 * @author Robert Koszewski
 */
public class DashboardController {

	private DashboardView dashboard_view;
	
	private SystemInfo sysinfo;

	@InitializeController
	public void initialize(WindowTemplate template, SharedSpace shared){
		// Initialize View
		dashboard_view = new DashboardView();
		
		// Initialize Menu Sections
		MenuSection dashboard_menu_section = new MenuSection("General");
		MenuEntry dashboard_menu = new MenuEntry("Dashboard","/");
		dashboard_menu.setFAIcon("tachometer");
		dashboard_menu_section.addMenuEntries(dashboard_menu);
		template.addMenuElement("dashboard", 10, dashboard_menu_section);
		
		// Initialize Classes
		sysinfo = new SystemInfo();
	}
	
	@Route(url="/")
	public Content getDashboard(Response res, Request req, SharedSpace shared, UIControl uicontrol){
		dashboard_view.updateData(shared, sysinfo);
		return dashboard_view.getContent();
	}
}
