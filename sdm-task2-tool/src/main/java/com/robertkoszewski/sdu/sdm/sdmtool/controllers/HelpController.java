/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.controllers;

import com.robertkoszewski.sdu.sdm.sdmtool.views.ManualView;
import com.robertkoszewski.wui.Content;
import com.robertkoszewski.wui.InitializeController;
import com.robertkoszewski.wui.Route;
import com.robertkoszewski.wui.WindowTemplate;
import com.robertkoszewski.wui.widgets.MenuEntry;
import com.robertkoszewski.wui.widgets.MenuSection;

/**
 * Help Controller
 * @author Robert Koszewski
 */
public class HelpController {
	
	private ManualView manual_view;

	@InitializeController
	public void initialize(WindowTemplate template){
		// Section
		MenuSection menu_section = new MenuSection("Help");
		// Menu Entry - Start new research
		MenuEntry manual_menu = new MenuEntry("Manual","/manual");
		manual_menu.setFAIcon("book");
		menu_section.addMenuEntries(manual_menu);
		
		// Add it to the main template
		template.addMenuElement("help", 40, menu_section);
		
		manual_view = new ManualView();
	}
	
	@Route(url="/manual")
	public Content showManual(){
		return manual_view.getContent();
	}
}
