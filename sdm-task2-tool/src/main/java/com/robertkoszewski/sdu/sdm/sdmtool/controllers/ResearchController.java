/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.controllers;

import com.robertkoszewski.sdu.sdm.sdmtool.research.Research;
import com.robertkoszewski.sdu.sdm.sdmtool.research.ResearchManager;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.ProviderManager;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.SocialProvider;
import com.robertkoszewski.sdu.sdm.sdmtool.views.*;
import com.robertkoszewski.wui.*;
import com.robertkoszewski.wui.Notification.Type;
import com.robertkoszewski.wui.widgets.*;

import spark.Request;

/**
 * Research Controller
 * @author Robert Koszewski
 */
public class ResearchController {
	
	/*
     * Sentiment Analysis tools for Java
     * - OpenNLP: http://technobium.com/sentiment-analysis-using-opennlp-document-categorizer/ (https://opennlp.apache.org/)
     * - LingPipe: http://alias-i.com/lingpipe/demos/tutorial/sentiment/read-me.html (http://alias-i.com/lingpipe/)
     * */
	
	// Views
	NewResearchView new_research_view; 				// New Research View
	ResearchListView research_list_view; 			// Research List View
	ResearchNotFoundView research_not_found_view; 	// Research Not Found View
	
	// Managers
	ResearchManager research_manager;

	@InitializeController
	public void initialize(WindowTemplate template, Persistence persistence, SharedSpace shared){
		// Start Views
		new_research_view = new NewResearchView();
		research_list_view = new ResearchListView(shared);
		research_not_found_view = new ResearchNotFoundView();
				
		// Start UI
		// Section
		MenuSection menu_section = new MenuSection("Research");
		// Menu Entry - Start new research
		MenuEntry newresearch_menu = new MenuEntry("Start new research","/newresearch");
		newresearch_menu.setFAIcon("plus");
		menu_section.addMenuEntries(newresearch_menu);
		// Menu Entry - View ongoing researches
		MenuEntry research_menu = new MenuEntry("View ongoing researches","/research");
		research_menu.setFAIcon("search");
		menu_section.addMenuEntries(research_menu);
		
		// Add it to the main template
		template.addMenuElement("research", 20, menu_section);
		
		// Start Research Manager
		research_manager = new ResearchManager(persistence);
		shared.addObject("research_manager", research_manager);
		research_list_view.updateResearchList(research_manager);
	}
	
	
	/**
	 * Create a New Research
	 * @param req
	 * @param shared
	 * @return
	 */
	@Route(url="/newresearch")
	public Content newResearch(Request req, SharedSpace shared, UIControl uicontrol){

		// Submit new research
		if(Utils.hasDataID(req, "newresearch") && Utils.hasRequestValueNotNull(req, "research_id", "research_name")){
			
			Research research = new Research(req.queryParams("research_id").trim(), req.queryParams("research_name").trim());
			
			if(research_manager.addResearch(req.queryParams("research_id").trim(), research, false)){
				// Success
				research_list_view.updateResearchList(research_manager);
				uicontrol.addNotification("New Research Created", "The research with ID '"+req.queryParams("research_id").trim()+"' has been created successfully", Type.SUCCESS);
				uicontrol.setRedirectURL("/research/"+req.queryParams("research_id").trim());
				research_manager.persistResearches();
				
			}else{
				// Error                   Research not created
				uicontrol.addNotification("Research Not Created", "ERROR: The Research ID already exists, please choose a different one", Type.ERROR);
			}
		}

		return new_research_view.getContent();
	}
	
	
	/**
	 * View List of Ongoing Researches
	 * @param req
	 * @param shared
	 * @param uicontrol
	 * @return
	 */
	@Route(url="/research")
	public Content showResearchList(Request req, SharedSpace shared, UIControl uicontrol){
		return research_list_view.getContent();
	}
	
	
	/**
	 * View List of Ongoing Researches
	 * @param req
	 * @param shared
	 * @param uicontrol
	 * @return
	 * @throws Exception 
	 */
	@Route(url="/research/:researchid")
	public Content showResearch(Request req, SharedSpace shared, UIControl uicontrol) throws Exception{
		Research research = research_manager.getResearch(req.params(":researchid"));
		if(research == null){
			// Research Not Found
			return research_not_found_view.getContent();
	
		}else{
			// Valid Research
			
			if(!research.isRunning()){
				// Handle Actions: Research Stopped
				
				// Add New Data Provider
				if(Utils.hasDataID(req, "addProvider") && Utils.hasRequestValueNotNull(req, "providerID")){
					String providerID = req.queryParams("providerID");
					try{
						SocialProvider provider = ProviderManager.getNewProviderInstance(providerID);
						research.addDataProvider(provider);
						uicontrol.addNotification("New Provider Added", provider.getProviderName()+" has been added successfully", Notification.Type.SUCCESS);
						research_manager.persistResearches();
						
					}catch(Exception e){
						uicontrol.addNotification("Provider not added", "ERROR: The selected provider could not be added. Reason: '"+e.getCause().getMessage(), Notification.Type.ERROR);
						
					}
				}else
				
				// Remove Data Provider
				if(Utils.hasDataID(req, "removeProvider") && Utils.hasRequestValueNotNull(req, "providerID")){
					String providerID = req.queryParams("providerID");
					try{
						research.removeDataProvider(providerID);
						uicontrol.addNotification("Provider Removed", " The provider has been removed successfully", Notification.Type.SUCCESS);
						research_manager.persistResearches();
						
					}catch(Exception e){
						uicontrol.addNotification("Provider not removed", "ERROR: The selected provider could not be removed or has been already removed.", Notification.Type.ERROR);
						
					}
				} else
					
				// Update Provider Data	
				if(Utils.hasDataID(req, "updateProviderData") && Utils.hasRequestValueNotNull(req, "providerUUID")){
					String providerID = req.queryParams("providerUUID");
					
					try{
						research.updateDataProvider(providerID, req);
						uicontrol.addNotification("Success", "Provider has been updated successfully", Notification.Type.SUCCESS);
						research_manager.persistResearches();
						
					}catch(Exception e){
						uicontrol.addNotification("ERROR", e.getMessage(), Notification.Type.ERROR);
					}
					
				}else
				
				// Enable or Disable Data Processors
				if(Utils.hasDataID(req, "enableProcessor") && Utils.hasRequestValueNotNull(req, "providerID", "action")){
					String providerID = req.queryParams("providerID");
					boolean enabled = (req.queryParams("action").equals("enable")?true:false);
					research.setDataProviderEnabled(providerID, enabled);
					uicontrol.addNotification("Success", "Data Provider status has been updated", Notification.Type.SUCCESS);
					research_manager.persistResearches();
					
				}else
				
				// Start Research
				if(Utils.hasDataID(req, "controlStart")){
					research.setRunning(true, shared, uicontrol);
					research_list_view.updateResearchList(research_manager);
						
				}else
					
				if(Utils.hasDataID(req, "controlRemove")){
					research_manager.removeResearch(research);
					uicontrol.setRedirectURL("/research");
					research_manager.persistResearches();
					research_list_view.updateResearchList(research_manager);
					return research_not_found_view.getContent();
				}	
				
				
			}else{
				// Handle Actions: Research Running
				
				// Stop Research
				if(Utils.hasDataID(req, "controlStop")){
					research.setRunning(false, shared, uicontrol);
					research_list_view.updateResearchList(research_manager);
						
				}
				
			}
			
			return research.getContent();
		}
	}
}
