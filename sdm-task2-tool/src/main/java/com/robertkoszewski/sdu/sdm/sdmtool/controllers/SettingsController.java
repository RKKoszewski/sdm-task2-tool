/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.controllers;

import com.google.gson.annotations.Expose;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.twitter.TwitterAccessTokenRequest;
import com.robertkoszewski.sdu.sdm.sdmtool.views.SettingsView;
import com.robertkoszewski.wui.Content;
import com.robertkoszewski.wui.InitializeController;
import com.robertkoszewski.wui.Notification;
import com.robertkoszewski.wui.Persistence;
import com.robertkoszewski.wui.Route;
import com.robertkoszewski.wui.SharedSpace;
import com.robertkoszewski.wui.UIControl;
import com.robertkoszewski.wui.Utils;
import com.robertkoszewski.wui.WindowTemplate;
import com.robertkoszewski.wui.widgets.MenuEntry;
import com.robertkoszewski.wui.widgets.MenuSection;

import spark.Request;
import twitter4j.auth.AccessToken;

/**
 * Settings Controller
 * @author Robert Koszewski
 */
public class SettingsController {
	
	public final static String twitter_settings_id = "twitter_provider";
	
	private SettingsView view;

	@InitializeController
	public void initialize(WindowTemplate template, Persistence persistence, SharedSpace shared){
		// Section
		MenuSection menu_section = new MenuSection("Settings");
		// Menu Entry - Start new research
		MenuEntry settings_menu = new MenuEntry("Settings","/settings");
		settings_menu.setFAIcon("cog");
		menu_section.addMenuEntries(settings_menu);
		
		// Add it to the main template
		template.addMenuElement("settings", 30, menu_section);
		
		try {
			shared.addObject(twitter_settings_id, persistence.readObject(twitter_settings_id, TwitterProviderSetting.class));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		view = new SettingsView();
		
		try{
			TwitterProviderSetting tws = shared.getObject(twitter_settings_id, TwitterProviderSetting.class);
			
			if(tws != null)
				view.setTwitterAuthenticated(true, tws.screen_name);
			else
				view.setTwitterAuthenticated(false, null);
		}catch(Exception e){
			view.setTwitterAuthenticated(false, null);
		}
	}
	
	TwitterAccessTokenRequest tw_token_request = null;
	
	@Route(url="/settings")
	public Content showConfiguration(Request req, UIControl control, Persistence pers, SharedSpace shared){
		
		if(Utils.hasDataID(req, "twitterStartAuth")){
			tw_token_request = new TwitterAccessTokenRequest();
			try{
				tw_token_request.startAuth();
				view.enableTwitterPinMode();
			}catch(Exception e){
				e.printStackTrace();
			}

		} else
			
		if(Utils.hasDataID(req, "twitterPinAuth") && Utils.hasRequestValueNotNull(req, "pin")){
			if(tw_token_request != null){
				try{
					AccessToken token = tw_token_request.finishAuth(req.queryParams("pin").trim());
					
					TwitterProviderSetting s = new TwitterProviderSetting();
					s.oauth_access_token = token.getToken();
					s.oauth_access_secret = token.getTokenSecret();
					s.screen_name = token.getScreenName();
					
					pers.writeObject(twitter_settings_id, s);
					shared.addObject(twitter_settings_id, s);
					
					view.setTwitterAuthenticated(true, s.screen_name);
				
				}catch(Exception e){
					control.addNotification("ERROR", e.getMessage(), Notification.Type.ERROR);
				}
				
				
			}else{
				control.addNotification("ERROR", "Unexpected state", Notification.Type.ERROR);
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		return view.getContent();
		
		
		
	}
	
	/**
	 * Twitter Provider Settings
	 * @author Robert Koszewski
	 */
	public class TwitterProviderSetting{
		@Expose
		public String screen_name;
		@Expose
		public String oauth_access_token;
		@Expose
		public String oauth_access_secret;
	}
}
