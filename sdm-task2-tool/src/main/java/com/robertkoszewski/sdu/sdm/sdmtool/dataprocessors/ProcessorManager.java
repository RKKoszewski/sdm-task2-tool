/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.dataprocessors;

/**
 * Data Processor Manager
 * @author Robert Koszewski
 */
public class ProcessorManager {
	// DIRTY DIRTY WORKAROUND. TODO: Fix this in the future to a more dynamic approach.
	public static Class<?>[] processor_list = new Class<?>[]{SimpleCounter.class, SentimentAnalysis.class};
	
	/**
	 * Get all available providers
	 * @return
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	public static DataProcessor[] getProcessorInstances() throws InstantiationException, IllegalAccessException{
		DataProcessor[] processor_instances = new DataProcessor[processor_list.length];
		
		int i = 0;
		for(Class<?> p: processor_list){
			processor_instances[i++] = (DataProcessor) p.newInstance();
		}

		return processor_instances;
	}
}
