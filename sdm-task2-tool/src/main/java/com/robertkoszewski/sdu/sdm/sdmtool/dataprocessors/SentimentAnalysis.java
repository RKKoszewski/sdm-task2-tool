/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.dataprocessors;

import java.awt.Color;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;

import com.robertkoszewski.sdu.sdm.sdmtool.lingtools.LingTools;
import com.robertkoszewski.sdu.sdm.sdmtool.lingtools.LinguisticPolarity;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.UserPostData;
import com.robertkoszewski.structs.LimitedQueue;
import com.robertkoszewski.wui.widgets.*;

/**
 * General Feeling Data Processor
 * @author Robert Koszewski
 */
public class SentimentAnalysis implements DataProcessor{
	
	private boolean enabled = false;
	private LinguisticPolarity polarity;
	
	private int positive = 0;
	private int negative = 0;
	
	private int last_minute = -1;
	
	LiquidRow row;
	CountTile count_tile_positive;
	CountTile count_tile_negative;
	LineChart chart_seconds;
	LineChart chart_minutes;

	@Override
	public void initialize() {
		row = new LiquidRow();
		
		// Chart Seconds
		Panel chart_panel = new Panel("General Sentiment: Per Second");
		chart_seconds = new LineChart();
		chart_panel.addElement(chart_seconds);
		row.addElement(12, chart_panel);
		
		// Chart Minutes
		Panel chart_panel2 = new Panel("General Sentiment: Per Minute");
		chart_minutes = new LineChart();
		chart_panel2.addElement(chart_minutes);
		row.addElement(12, chart_panel2);
		
		// Count Tile Row
		CountTileRow count_tile_row = new CountTileRow();
		
		// Count Positive
		count_tile_positive = new CountTile("hashtag", "Positive Posts", "");
		count_tile_row.addElement(count_tile_positive);
		row.addElement(12, count_tile_row);
		
		// Count Positive
		count_tile_negative = new CountTile("hashtag", "Negative Posts", "");
		count_tile_row.addElement(count_tile_negative);
		
		last_minute = LocalDateTime.now().getMinute();
		
		polarity = LingTools.getTrainedIntance();
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String getName() {
		return "Sentiment Analysis";
	}

	@Override
	public String getFAIcon() {
		return "users";
	}

	// Per Second List
	//private int last_second = -1;
	private int positive_second = 0;
	private int negative_second = 0;
	private LimitedQueue<Integer> list_positive_second 	= new LimitedQueue<Integer>(60);
	private LimitedQueue<Integer> list_negative_second 	= new LimitedQueue<Integer>(60);
	private LimitedQueue<String> list_label_second 		= new LimitedQueue<String>(60);
	
	// Per Minute
	
	private int positive_minute = 0;
	private int negative_minute = 0;
	private LimitedQueue<Integer> list_positive_minute 	= new LimitedQueue<Integer>(60);
	private LimitedQueue<Integer> list_negative_minute 	= new LimitedQueue<Integer>(60);
	private LimitedQueue<String> list_label_minute 		= new LimitedQueue<String>(60);
	
//	LimitedQueue<Integer> limited = new LimitedQueue<Integer>(4);
//	LimitedQueue<String> lstr = new LimitedQueue<String>(4);
	
	
	
	@Override
	public void processData(List<UserPostData> data) {
		Iterator<UserPostData> di = data.iterator();
		while(di.hasNext()){
			UserPostData post = di.next();
			String sentiment = polarity.clasify(post.commentData);
			System.out.println("-- ["+sentiment+"] -> "+post.commentData);
			
			switch(sentiment){
			case "pos":
				positive++;
				positive_second++;
				positive_minute++;
				break;
			case "neg":
				negative++;
				negative_second++;
				negative_minute++;
				break;
			}
		}
		
		// Process Seconds
		list_positive_second.add(positive_second);
		positive_second = 0;
		list_negative_second.add(negative_second);
		negative_second = 0;
		list_label_second.add(LocalDateTime.now().getSecond()+"");
		chart_seconds.setLabels(list_label_second.toArray(new String[0]));
		chart_seconds.addDataset("pos", "Positive", queueToPrimaryInt(list_positive_second), Color.green.darker());
		chart_seconds.addDataset("neg", "Negative", queueToPrimaryInt(list_negative_second), Color.red.darker());
		
		// Process Minutes
		int minute = LocalDateTime.now().getMinute();
		if(minute != last_minute){
			
			list_positive_minute.add(positive_minute);
			positive_minute = 0;
			list_negative_minute.add(negative_minute);
			negative_minute = 0;
			list_label_minute.add(minute+"");
			chart_minutes.setLabels(list_label_minute.toArray(new String[0]));
			chart_minutes.addDataset("pos", "Positive", queueToPrimaryInt(list_positive_minute), Color.green.darker());
			chart_minutes.addDataset("neg", "Negative", queueToPrimaryInt(list_negative_minute), Color.red.darker());
			
			last_minute = minute;
		}
		
		count_tile_positive.setData(positive+"");
		count_tile_negative.setData(negative+"");
	}

	/**
	 * Generate Primary Elements from Queue Element
	 * @param limited_queue
	 * @return
	 */
	private int[] queueToPrimaryInt(LimitedQueue<Integer> limited_queue){
		
		int[] primary_array = new int[limited_queue.size()];
		Iterator<Integer> i = limited_queue.iterator();
		int n = 0;
		while(i.hasNext()){
			primary_array[n++] = i.next();
		}
		
		return primary_array;
	}
	
	@Override
	public LiquidRow getVisualData() {
		return row;
	}

}
