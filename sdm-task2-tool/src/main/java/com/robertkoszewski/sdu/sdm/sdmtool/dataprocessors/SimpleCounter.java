/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.dataprocessors;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.UserPostData;
import com.robertkoszewski.wui.widgets.*;

/**
 * Simple Data Counter
 * @author Robert Koszewski
 *
 */
public class SimpleCounter implements DataProcessor{
	
	// Enabled?
	private boolean enabled = false;
	
	// Variables
	private double total_count = 0;
	
	private int last_minute = -1;
	private int last_minute_count = 0;
	
	private int last_hour = -1;
	private int last_hour_count = 0;
	
	private int minute_count = 0;
	private int hour_count = 0;
	
	private double count_reposts = 0;
	
	// Helpers
	private static final DecimalFormat decimalFormat = new DecimalFormat("#");
	
	// Row
	private LiquidRow row;
	
	// Elements
	CountTile total_count_tile;
	CountTile lastmin_count_tile;
	CountTile lasthour_count_tile;
	CountTile avgmin_count_tile;
	CountTile avghour_count_tile;
	CountTile reposts_count_tile;
	
	@Override
	public void initialize() {
		row = new LiquidRow();

		// Count Tile Row
		CountTileRow count_tile_row = new CountTileRow();
		
		// Total Count
		total_count_tile = new CountTile("hashtag", "Total Post Count", decimalFormat.format(total_count));
		count_tile_row.addElement(total_count_tile);
		row.addElement(12, count_tile_row);
		
		// Posts during last minute
		lastmin_count_tile = new CountTile("clock-o", "Posts During Last Minute", last_minute_count+"");
		count_tile_row.addElement(lastmin_count_tile);
		
		// Posts during last hour
		lasthour_count_tile = new CountTile("calendar", "Posts During Last Hour", last_hour_count+"");
		count_tile_row.addElement(lasthour_count_tile);
		
		// Posts during last minute
		avgmin_count_tile = new CountTile("star-half", "Avg. Posts Per Minute", "0");
		count_tile_row.addElement(avgmin_count_tile);
		
		// Posts during last hour
		avghour_count_tile = new CountTile("star", "Avg. Posts Per Hour", "0");
		count_tile_row.addElement(avghour_count_tile);
		
		// Reposts
		reposts_count_tile = new CountTile("reply", "Number of Reposts", "0");
		count_tile_row.addElement(reposts_count_tile);
		
		// Update Start Values
		last_hour = LocalDateTime.now().getHour();
		last_minute = LocalDateTime.now().getMinute();

	}

	@Override
	public String getName() {
		return "Simple Data Counters";
	}

	@Override
	public String getFAIcon() {
		return "sort-numeric-asc";
	}

	@Override
	public void processData(List<UserPostData> data) {
		// Total Post Count
		total_count += data.size();
		total_count_tile.setData(decimalFormat.format(total_count));
		
		// Count Reposts
		Iterator<UserPostData> i = data.iterator();
		while(i.hasNext()){
			if(i.next().commentData.startsWith("RT "))
				count_reposts++;
		}
		reposts_count_tile.setData(decimalFormat.format(count_reposts));
		
		// Posts Last Minute
		int minute = LocalDateTime.now().getMinute();
		last_minute_count += data.size();
		if(minute != last_minute){
			last_minute = minute;
			lastmin_count_tile.setData(decimalFormat.format(last_minute_count));
			minute_count++;
			
			// Process AVG per Minute
			double minute_avg = last_minute_count/minute_count;
			avgmin_count_tile.setData(decimalFormat.format(minute_avg));
			
			last_minute_count = 0;
		}
		
		if(minute_count==0)
			avgmin_count_tile.setData(decimalFormat.format(total_count));
		
		// Posts Last Hour
		int hour = LocalDateTime.now().getHour();
		last_hour_count += data.size();
		if(hour != last_hour){
			last_hour = hour;
			lasthour_count_tile.setData(last_hour_count+"");
			hour_count++;
			
			// Process AVG per Hour
			double hour_avg = last_hour_count/hour_count;
			avghour_count_tile.setData(decimalFormat.format(hour_avg));
			
			last_hour_count = 0;
		}
		
		if(hour_count==0)
			avghour_count_tile.setData(decimalFormat.format(total_count));

	}

	@Override
	public LiquidRow getVisualData() {
		return row;
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
