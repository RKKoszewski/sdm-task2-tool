/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.lingtools;

import java.io.File;
import com.robertkoszewski.sdu.sdm.sdmtool.Configuration;
import com.robertkoszewski.utils.UnZip;

public class LingTools {

	private static LinguisticPolarity lp_instance = null;
	private static String model_filename = "sentiment.model";
	
	synchronized public static LinguisticPolarity getIntance(){
		if(lp_instance == null){
			lp_instance = new LinguisticPolarity();
		}
		return lp_instance;
	}
	
	synchronized public static LinguisticPolarity getTrainedIntance(){
		LinguisticPolarity lp = getIntance();
		
		if(lp.isTrained())
			return lp;
		
		// Setup Working Directory
		String working_dir;
        if(System.getProperty("user.home") != null && !System.getProperty("user.home").equals(""))
        	working_dir = System.getProperty("user.home") + File.separatorChar + "." + Configuration.app_id + File.separatorChar;
        else
        	working_dir = "."+File.separatorChar;
		
        File model = new File(working_dir + model_filename);
        boolean model_loaded = false;
        
        if(model.exists()){
        	try {
				lp.loadModel(model);
				model_loaded = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
        
        if(!model_loaded){
        	UnZip zip = new UnZip();
        	zip.unzip(LingTools.class.getResourceAsStream("/training_resources/polarity_trainingset.zip"), working_dir+"trainingset");
        	
        	lp.train(new File(working_dir+"trainingset")); // Start Training
        	try {
        		// Write Training Set in Separate Thread
        		new Thread(){
        			@Override
        			public void run(){
        				try {
							lp.saveModel(new File(working_dir + model_filename));
						} catch (Exception e) {
							e.printStackTrace();
						}
        			}
        		}.start();

			} catch (Exception e) {
				e.printStackTrace();
			}
        }
        
        return lp;
	}
	
}
