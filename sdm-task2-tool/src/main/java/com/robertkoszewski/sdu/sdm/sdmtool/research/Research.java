/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.research;

import com.robertkoszewski.sdu.sdm.sdmtool.dataprocessors.DataProcessor;
import com.robertkoszewski.sdu.sdm.sdmtool.dataprocessors.ProcessorManager;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.LimitReachedException;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.ProviderManager;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.SocialProvider;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.UnsupportedQueryException;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.UserPostData;
import com.robertkoszewski.sdu.sdm.sdmtool.views.ResearchView;
import com.robertkoszewski.wui.Content;
import com.robertkoszewski.wui.Notification;
import com.robertkoszewski.wui.SharedSpace;
import com.robertkoszewski.wui.UIControl;

import spark.Request;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

/**
 * Research
 * @author Robert Koszewski
 */
public class Research {
	
	// View
	private ResearchView research_view;

	// Data
	private String research_id = "";
	private String research_description = "";

	// Providers and Processors
	private Map<UUID, SocialProvider> active_providers;
	private DataProcessor[] data_processors;

	// Research Thread
	private boolean isRunning = false;
	Thread research_thread;

	public Research(String research_id, String research_description){
		this.research_id = research_id;
		this.research_description = research_description;

		// Initialize Arrays
		active_providers = new HashMap<UUID, SocialProvider>();

		// Update View
		research_view = new ResearchView(research_id, research_description);
		research_view.setResearchID(research_id);
		research_view.setResearchDescription(research_description);
		
		research_view.setProviderList(ProviderManager.getProviders());
		
		// Enable all Data Processors
		try{
			data_processors = ProcessorManager.getProcessorInstances();
			research_view.setDataProcessorList(data_processors);
		}catch(Exception e){
			System.err.println("ERROR: The data providers could not be initialized.");
			e.printStackTrace();
		}
	}
	
	/**
	 * Start/Stop Providers
	 * @param start
	 */
	private void initProviders(boolean start, SharedSpace shared, UIControl control){
		Iterator<Entry<UUID, SocialProvider>> pi = active_providers.entrySet().iterator();
		while(pi.hasNext()){
			if(start){
				SocialProvider sp = pi.next().getValue();
				try{
					sp.start(shared);
				}catch(Exception e){
					control.addNotification("ERROR", "Failed to start provider: "+sp.getProviderName(), Notification.Type.ERROR);
				}
			}else{
				try{
					pi.next().getValue().stop();
				}catch(Exception e){}
			}
		}
	}
	
	/**
	 * Add Data Provider
	 * @param data_provider
	 */
	public void addDataProvider(SocialProvider data_provider){
		UUID uuid = UUID.randomUUID();
		active_providers.put(uuid, data_provider);
		data_provider.ProviderInit(uuid);
		research_view.setActiveProviderList(active_providers);
	}
	
	/**
	 * Remove a Active Data Provider
	 * @param id
	 * @throws Exception
	 */
	public void removeDataProvider(String id) throws Exception{
		UUID uuid = UUID.fromString(id);
		if(!active_providers.containsKey(uuid)){
			throw new Exception("Provider could not be found");
		}else{
			active_providers.remove(uuid);
		}
		research_view.setActiveProviderList(active_providers);
	}
	
	/**
	 * Update a Data Provider
	 * @param id
	 * @param req
	 * @throws Exception 
	 */
	public void updateDataProvider(String id, Request req) throws Exception{
		SocialProvider provider = active_providers.get(UUID.fromString(id));
		provider.configureProvider(req);
		research_view.updateActiveProviderTable();
	}
	
	/**
	 * Return the Research Description
	 * @return
	 */
	public String getResearchDescription(){
		return research_description;
	}
	
	/**
	 * Return the Research ID
	 * @return
	 */
	public String getResearchID(){
		return research_id;
	}
	
	/**
	 * Get Research View Content
	 * @return
	 */
	public Content getContent(){
		return research_view.getContent();
	}
	
	/**
	 * Returns if Research is Running or not
	 * @return
	 */
	public boolean isRunning(){
		return this.isRunning;
	}
	
	/**
	 * Change Research Running State
	 * @param running
	 */
	public void setRunning(boolean running, SharedSpace shared, UIControl control){
		
		// Initialize Data Processors
		for(DataProcessor dp: data_processors){
			if(dp.isEnabled())
				dp.initialize();
		}
		
		// Initialize Providers
		initProviders(running, shared, control);
		
		// Set Running State
		research_view.setRunningState(running, data_processors);
		this.isRunning = running;

		if(running){
			// Start
			research_thread = new Thread(){
				@Override
				public void run(){
					//System.out.println("Starting Research Thread");
					
					List<UserPostData> user_post_data = new ArrayList<UserPostData>();
					while(isRunning){
						// System.out.println("New Research Loop");
						
						// Gather Data
						Iterator<Entry<UUID, SocialProvider>> api = active_providers.entrySet().iterator();
						while(api.hasNext()){
							Entry<UUID, SocialProvider> ap = api.next();
							SocialProvider sp = ap.getValue();
							
							try {
								List<UserPostData> list = sp.getUserPosts();
								user_post_data.addAll(list);
							} catch (UnsupportedQueryException e) {
								e.printStackTrace();
							} catch (LimitReachedException e) {
								e.printStackTrace();
							} catch (Exception e) {
								e.printStackTrace();
							}
							
						}
						
						// Process Data
						if(!user_post_data.isEmpty()){
							// Run Processors only with new data
							for(DataProcessor dp: data_processors){
								if(dp.isEnabled())
									dp.processData(user_post_data);
							}
							
							// Clear Data
							user_post_data.clear();
						}
						
						try {
							//System.out.println("Waiting 1 seconds");
							Thread.sleep(1000);
						} catch (InterruptedException e) {}
					}
				}
			};
			research_thread.start();
			
		}else{
			research_thread.interrupt();
		}
		
	}
	
	/**
	 * Set Data Provider Enabled/Disabled status
	 * @param providerID
	 * @param enabled
	 */
	public void setDataProviderEnabled(String providerID, boolean enabled){
		for(DataProcessor p: data_processors){
			if(p.getClass().getName().equals(providerID)){
				p.setEnabled(enabled);
			}
		}
		research_view.setDataProcessorList(data_processors);
	}
	
	/**
	 * Return all Active Providers
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ResearchPersistence getResearchPersistence(){
		ResearchPersistence rp = new ResearchPersistence();
		rp.research_id = getResearchID();
		rp.research_description = getResearchDescription();
		
		// Providers
		rp.enabled_providers = new com.robertkoszewski.structs.Entry[this.active_providers.size()];
		Iterator<Entry<UUID, SocialProvider>> pi = this.active_providers.entrySet().iterator();
		int i = 0;
		while(pi.hasNext()){
			Entry<UUID, SocialProvider> p = pi.next();
			rp.enabled_providers[i++] = new com.robertkoszewski.structs.Entry<String, String>(p.getValue().getClass().getName(), p.getValue().getQuery());
		}
		
		// Processors
		ArrayList<String> processors = new ArrayList<String>();
		for(DataProcessor p: this.data_processors){
			if(p.isEnabled()){
				processors.add(p.getClass().getName());
			}
		}
		rp.enabled_processors = new String[processors.size()];
		rp.enabled_processors = processors.toArray(rp.enabled_processors);
		
		return rp;
	}
}
