/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.research;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.ProviderManager;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.SocialProvider;
import com.robertkoszewski.wui.Content;
import com.robertkoszewski.wui.Persistence;

/**
 * Research Manager
 * @author Robert Koszewski
 */
public class ResearchManager {
	
	private static final String persistence_id = "researches";
	private Persistence persistence;
	
	private Map<String, Research> research_list;

	public ResearchManager(Persistence persistence){

		research_list = new HashMap<String, Research>();
		
		// Read from Persistence file
		try{
			ResearchListPersistence p = persistence.readObject(persistence_id, ResearchListPersistence.class);
			Iterator<ResearchPersistence> pi = p.researches.iterator();
			while(pi.hasNext()){
				ResearchPersistence o = pi.next();
				Research research = new Research(o.research_id, o.research_description);

				// Add Data Providers
				for(Map.Entry<String, String> prov: o.enabled_providers){
					try{
						SocialProvider provider = ProviderManager.getNewProviderInstance(prov.getKey());
						provider.setQuery(prov.getValue());
						research.addDataProvider(provider);
						
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				
				// Set Data Processors
				for(String proc: o.enabled_processors){
					research.setDataProviderEnabled(proc, true);
				}
				
				this.addResearch(o.research_id, research, true);
			}
		}catch(Exception e){}

		this.persistence = persistence;
	}

	/**
	 * Persist Researches
	 */
	public void persistResearches(){
		ResearchListPersistence p = new ResearchListPersistence();
		p.researches = new ArrayList<ResearchPersistence>();
		
		Iterator<java.util.Map.Entry<String, Research>> it = research_list.entrySet().iterator();
		while(it.hasNext()){
			java.util.Map.Entry<String, Research> en = it.next();
			p.researches.add(en.getValue().getResearchPersistence());
		}

		try {
			persistence.writeObject(persistence_id, p);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Get a research with ID
	 * @param research_id
	 * @return
	 */
	public Research getResearch(String research_id){
		return research_list.get(research_id);
	}
	
	/**
	 * Return List of Researches
	 * @return
	 */
	public Set<java.util.Map.Entry<String, Research>> getResearchList(){
		return this.research_list.entrySet();
	}
	
	/**
	 * Add a New Research
	 * @param research_id
	 * @param research
	 * @param force_replace
	 * @return
	 */
	public boolean addResearch(String research_id, Research research, boolean force_replace){
		if(research_list.get(research_id)==null || force_replace){
			research_list.put(research_id, research);
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Get Research View
	 * @param research_id
	 * @return
	 */
	public Content getResearchView(String research_id){
		Research r = research_list.get(research_id);
		if(r == null)
			return null;
		else
			return r.getContent();
	}
	
	
	/**
	 * Return number of researches
	 * @return
	 */
	public int getResearchCount(){
		return research_list.size();
	}
	
	/**
	 * Return Count of Running Researches
	 * @return
	 */
	public int getRunningResearchCount(){
		int count = 0;
		Iterator<Research> i = research_list.values().iterator();
		while(i.hasNext()){
			if(i.next().isRunning())
				count++;
		}
		return count;
	}

	/**
	 * Remove a Research
	 * @param research
	 */
	public void removeResearch(Research research) {
		Iterator<Entry<String, Research>> i = research_list.entrySet().iterator();
		while(i.hasNext()){
			 Entry<String, Research> e = i.next();
			 if(e.getValue().equals(research)){
				 research_list.remove(e.getKey());
				 return;
			 }
		}
	}

}
