/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.socialproviders;

import org.joda.time.DateTime;

import com.robertkoszewski.utils.Utils;

/**
 * This Exception is thrown when an API's limit is reached and returns a timestamp
 * when next call can be launched. If the timestamp is -1
 * @author Robert Koszewski
 */
public class LimitReachedException extends Exception {

	private static final long serialVersionUID = -2888136982973749283L;
	private int next_allowed_time;
	
	public LimitReachedException(String cause, boolean indefinite){
		super(cause);
		this.next_allowed_time = -1; // To UNIX time stamp
	}
	
	public LimitReachedException(String cause, int seconds_timeout){
		super(cause);
		this.next_allowed_time = (int) (Utils.getTimestamp() + seconds_timeout); // To UNIX time stamp
	}
	
	public LimitReachedException(String cause, DateTime next_allowed_time){
		super(cause);
		this.next_allowed_time = (int) (next_allowed_time.getMillis() / 1000L); // To UNIX time stamp
	}
	
	/**
	 * Returns the next allowed time as Unix Timestamp
	 * @return
	 */
	public long getNextAllowedTimeUnix(){
		return next_allowed_time;
	}

}
