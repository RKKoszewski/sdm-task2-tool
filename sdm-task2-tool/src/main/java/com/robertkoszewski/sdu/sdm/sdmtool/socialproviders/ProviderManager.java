/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.socialproviders;

import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.twitter.TwitterProvider;

/**
 * Provider Manager
 * @author Robert Koszewski
 */
public class ProviderManager {

	// DIRTY DIRTY WORKAROUND. TODO: Fix this
	public static SocialProvider[] provider_list = new SocialProvider[]{new TwitterProvider()};
	
	/**
	 * Get all available providers
	 * @return
	 */
	public static SocialProvider[] getProviders(){
		return provider_list;
	}
	
	/**
	 * Get a new Provider Instance
	 * @param provider_id
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 */
	public static SocialProvider getNewProviderInstance(String provider_id) throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		
		for(SocialProvider prov: provider_list){
			if(prov.getClass().getName().equals(provider_id))
				return prov.getClass().newInstance();
		}
		
		throw new ClassNotFoundException("Could not find the provider with id '"+provider_id+"'.");
	}
}
