/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.socialproviders;

import java.util.List;
import java.util.UUID;

import com.robertkoszewski.wui.SharedSpace;
import com.robertkoszewski.wui.widgets.Form;

import spark.Request;

/**
 * Social Provider Interface
 * @author Robert Koszewski
 */
public interface SocialProvider {

	/**
	 * Initializes the Provider
	 */
	public void ProviderInit(UUID providerUID);
	
	/**
	 * Start Provider
	 */
	public void start(SharedSpace shared);
	
	/**
	 * Stop Provider
	 */
	public void stop();
	
	/**
	 * Returns User Posts using a SocialQuery as input
	 * @param query
	 * @return UserPost List
	 */
	public List<UserPostData> getUserPosts(/*SocialQuery query*/) throws UnsupportedQueryException, LimitReachedException, Exception;
	
	/**
	 * Set Query (TODO: To be replaced)
	 * @param query
	 */
	public void setQuery(String query);
	
	/**
	 * Get Query (TODO: To be replaced)
	 * @param query
	 * @return
	 */
	public String getQuery();
	
	// Icon Related
	/**
	 * Get a Human readable Provider Name
	 * @return
	 */
	public String getProviderName();
	
	/**
	 * Get a Font Awesome Icon representing the Provider. More info at https://goo.gl/FmVQBB. Return NULL if no icon.
	 * @return
	 */
	public String getProviderFAIcon();
	
	/**
	 * Return a Provider UI Form for it's configuration
	 * @param providerUID
	 * @return
	 */
	public Form getProviderConfigForm();

	/**
	 * Configure Provider
	 * @throws Exception 
	 */
	public void configureProvider(Request req) throws Exception;
}
