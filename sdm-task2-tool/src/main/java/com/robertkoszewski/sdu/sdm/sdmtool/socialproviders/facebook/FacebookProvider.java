/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.facebook;

import facebook4j.*;
import facebook4j.conf.ConfigurationBuilder;
import spark.Request;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.LimitReachedException;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.SocialProvider;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.SocialQuery;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.UnsupportedQueryException;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.UserPostData;
import com.robertkoszewski.wui.SharedSpace;
import com.robertkoszewski.wui.widgets.Form;

public class FacebookProvider implements SocialProvider {

    private Facebook fbFactoryInstance;
    private UserPostData userPostData[];
    private Map<String,Object> fbMetadataMap;
    
    public void ProviderInit(UUID u) {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthAppId(FacebookQuery.FacebookAppID)
                .setOAuthAppSecret(FacebookQuery.FacebookAppSecret)
                .setOAuthAccessToken(FacebookQuery.FacebookAccessToken)
                .setOAuthPermissions(FacebookQuery.FacebookPermissins);
        FacebookFactory fbFactory = new FacebookFactory(cb.build());
        fbFactoryInstance = fbFactory.getInstance();
    }

 
    public List<UserPostData> getUserPosts(SocialQuery query) throws UnsupportedQueryException, LimitReachedException, Exception {

        // Check if the SocialQuery is a Facebook Query
        if(!(query.getClass() == FacebookQuery.class)){
            throw new UnsupportedQueryException("The provided SocialQuery instance is of type " + query.getClass()+
                    " instead of "+ FacebookQuery.class.getName());
        }
        FacebookQuery fbQuery = (FacebookQuery) query;
        ResponseList<Post> searchPosts = fbFactoryInstance.searchPosts(fbQuery.search_string);

        for (int i=0;i<searchPosts.size();i++) {
            Post fbpost = searchPosts.get(i);
            //userPostData[i].setUserID(fbpost.);
           // userPostData[i].setCommentData(fbpost.getMessage());
           // userPostData[i].setTimestamp(fbpost.getCreatedTime());
            fbpost.getMetadata().getType();
           // fbMetadataMap.put(fbpost.getMetadata().getType(),fbpost.getMetadata());
            //userPostData[i].setMetaData(fbMetadataMap);
            //userPostData[i].setUserName(fbpost.getName());
        }
        // Facebook Query Object

        // Do  some API magic here...
        // Information about Facebook4J here: http://facebook4j.github.io/en/index.html

        // If API limit is reached throw an LimitReachedException specifying a timeout in seconds

        // Return an array of UserPosts
       // return userPostData;
        return null;
    }

	@Override
	public String getProviderName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProviderFAIcon() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Form getProviderConfigForm() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void configureProvider(Request req) throws Exception {
		// TODO Auto-generated method stub
		
	}


	@Override
	public List<UserPostData> getUserPosts() throws UnsupportedQueryException, LimitReachedException, Exception {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void setQuery(String query) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public String getQuery() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}


	public void ProviderInit(UUID providerUID, SharedSpace shared) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void start(SharedSpace shared) {
		// TODO Auto-generated method stub
		
	}


}
