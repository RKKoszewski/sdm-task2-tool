/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.facebook;

import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.SocialQuery;

/**
 * Use this Class to pass Facebook Queries and settings
 * @author
 *
 */
public class FacebookQuery implements SocialQuery {

	// TODO: This is just an example implementation
	public static String FacebookAppID = "1804740569803804";
	public static String FacebookAppSecret = "1446c677655507cd3aca93ba8cbb5443";
	public static String FacebookAccessToken = "e0dfe9f16ed5cc51f74664c268a07025";
	public static String FacebookPermissins = "public_profile,use_friends,use_about_me,user_likes,user_location,user_posts,user_relationships";
	public String search_string;

	public int max_results; // TODO: Just a demo
	public int result_page; // TODO: Just a demo
	@Override
	public void setSearchQuery(String query) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public String getSearchQuery() {
		// TODO Auto-generated method stub
		return null;
	}

}
