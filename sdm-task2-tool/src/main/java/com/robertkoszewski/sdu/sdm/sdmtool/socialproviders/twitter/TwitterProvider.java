/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.twitter;


import com.robertkoszewski.sdu.sdm.sdmtool.controllers.SettingsController;
import com.robertkoszewski.sdu.sdm.sdmtool.controllers.SettingsController.TwitterProviderSetting;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.*;
import com.robertkoszewski.structs.CumulativeList;
import com.robertkoszewski.wui.SharedSpace;
import com.robertkoszewski.wui.Utils;
import com.robertkoszewski.wui.widgets.Form;
import com.robertkoszewski.wui.widgets.TextInput;
import spark.Request;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;
import java.util.List;
import java.util.UUID;

public class TwitterProvider implements SocialProvider {
	
	// API Keys
	public static String consumer_key = "U7qeZLrsF7VIy4Lmw6ONyhRPp";
	public static String consumer_secret = "ASEneQw2QVRAKnk0iqdD7R1rbtoIludgI3fkwtfW0JjNfe2sjB";
	
	private Form config_form;

	// Data
	CumulativeList<UserPostData> list = new CumulativeList<UserPostData>();
	
	TwitterStream twitterStream;
	
	private String search_query = "";

	/**
	 * Replace newlines and tabs in text with escaped versions to making printing cleaner
	 *
	 * @param text The text of a tweet, sometimes with embedded newlines and tabs
	 * @return The text passed in, but with the newlines and tabs replaced
	 */
	public static String cleanText(String text) {
		text = text.replace("\n", "\\n");
		text = text.replace("\t", "\\t");

		return text;
	}

	@Override
	public void ProviderInit(UUID providerUID) {
		config_form = new Form("updateProviderData");
		config_form.forceVerticalLayout(true);
		config_form.addTextInput("search_query", "Search Query", search_query, TextInput.Type.text, true);
		config_form.appendTextInput("providerUUID", null, providerUID.toString(), TextInput.Type.hidden, false);
		config_form.addSubmitButton("send", "Update Value", false);  
	}
	
	private StatusListener listener = new StatusListener(){
        public void onStatus(Status status) {
            //System.out.println(status.getUser().getName() + " : " + status.getText());
            UserPostData userPostData = new UserPostData(status.getCreatedAt(), status.getId(), status.getUser().getScreenName(), cleanText(status.getText()));
            list.add(userPostData);
            
        }
        public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {}
        public void onTrackLimitationNotice(int numberOfLimitedStatuses) {}
        public void onException(Exception ex) {
            ex.printStackTrace();
        }
		@Override
		public void onScrubGeo(long arg0, long arg1) {}
		@Override
		public void onStallWarning(StallWarning arg0) {}
    };
	
	@Override
	public void start(SharedSpace shared){

		TwitterProviderSetting settings = (TwitterProviderSetting) shared.getObject(SettingsController.twitter_settings_id);
		
		twitterStream = getTwitterStream(settings.oauth_access_token, settings.oauth_access_secret);
	    twitterStream.addListener(listener);
		
		FilterQuery filter = new FilterQuery();
		filter.language("en");
		filter.track(this.search_query);
		//filter.filterLevel("medium");
		twitterStream.filter(filter);
	}
	
	@Override
	public void stop(){
		twitterStream.shutdown();
	}


	public static TwitterStream getTwitterStream(String oauth_access_token, String oauth_access_secret)
	{

		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setHttpConnectionTimeout(15*1000).setHttpReadTimeout(60*1000);
		cb.setOAuthConsumerKey(consumer_key);
		cb.setOAuthConsumerSecret(consumer_secret);
		cb.setOAuthAccessToken(oauth_access_token);
		cb.setOAuthAccessTokenSecret(oauth_access_secret);

		//	And create the Twitter object!
		return new TwitterStreamFactory(cb.build()).getInstance();

	}

	@Override
	public List<UserPostData> getUserPosts() throws UnsupportedQueryException, LimitReachedException, Exception {

		return list.getList();
	}

	@Override
	public String getProviderName() {
		return "Twitter Provider";
	}

	@Override
	public String getProviderFAIcon() {
		return "twitter";
	}

	@Override
	public Form getProviderConfigForm() {
		return config_form;
	}

	@Override
	public void configureProvider(Request req) throws Exception {

		if(Utils.hasRequestValueNotNull(req, "search_query")){
			String search_query = req.queryParams("search_query");
			
			if(search_query == null || search_query.trim().equals("")){
				throw new Exception("ERROR: Invalid search query.");
			}
			this.search_query = search_query;
			config_form.updateValue("search_query", search_query);
			
		}else{
			throw new Exception("ERROR: Missing parameter search_query");
		}
	}

	@Override
	public void setQuery(String query) {
		this.search_query = query;
	}

	@Override
	public String getQuery() {
		return this.search_query;
	}

}

