/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.twitter;


import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.*;
import com.robertkoszewski.wui.SharedSpace;
import com.robertkoszewski.wui.Utils;
import com.robertkoszewski.wui.widgets.Form;
import com.robertkoszewski.wui.widgets.TextInput;

import spark.Request;
import twitter4j.*;
import twitter4j.auth.OAuth2Token;
import twitter4j.conf.ConfigurationBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class TwitterProviderLegacy implements SocialProvider {
	
	private Form config_form;

	
	private static final int TWEETS_PER_QUERY = 100;
	private static final int MAX_QUERIES = 5;
	//private int totalTweets = 0;
	private long maxID = -1;
	
	private String search_query = "";

	/**
	 * Replace newlines and tabs in text with escaped versions to making printing cleaner
	 *
	 * @param text The text of a tweet, sometimes with embedded newlines and tabs
	 * @return The text passed in, but with the newlines and tabs replaced
	 */
	public static String cleanText(String text) {
		text = text.replace("\n", "\\n");
		text = text.replace("\t", "\\t");

		return text;
	}

	@Override
	public void ProviderInit(UUID providerUID) {
		config_form = new Form("updateProviderData");
		config_form.forceVerticalLayout(true);
		config_form.addTextInput("search_query", "Search Query", search_query, TextInput.Type.text, true);
		config_form.appendTextInput("providerUUID", null, providerUID.toString(), TextInput.Type.hidden, false);
		config_form.addSubmitButton("send", "Update Value", false);
	}

	public static OAuth2Token getOAuth2Token()
	{
		OAuth2Token token = null;
		ConfigurationBuilder cb;

		cb = new ConfigurationBuilder();
		cb.setApplicationOnlyAuthEnabled(true);

		cb.setOAuthConsumerKey("gUte7CbntXlkuvffltcGMcwcD").setOAuthConsumerSecret("QV8cNxLEjbpbEpc5VvI9jWz6veYjUesLNoG5P4IoKHItia8JnA");

		try
		{
			token = new TwitterFactory(cb.build()).getInstance().getOAuth2Token();
		}
		catch (Exception e)
		{
			System.out.println("Could not get OAuth2 token");
			e.printStackTrace();
			System.exit(0);
		}

		return token;
	}

	/**
	 * Get a fully application-authenticated Twitter object useful for making subsequent calls.
	 *
	 * @return	Twitter4J Twitter object that's ready for API calls
	 */
	public static Twitter getTwitter()
	{
		OAuth2Token token;

		//	First step, get a "bearer" token that can be used for our requests
		token = getOAuth2Token();

		//	Now, configure our new Twitter object to use application authentication and provide it with
		//	our CONSUMER key and secret and the bearer token we got back from Twitter
		ConfigurationBuilder cb = new ConfigurationBuilder();

		cb.setApplicationOnlyAuthEnabled(true);

		cb.setOAuthConsumerKey("gUte7CbntXlkuvffltcGMcwcD");
		cb.setOAuthConsumerSecret("QV8cNxLEjbpbEpc5VvI9jWz6veYjUesLNoG5P4IoKHItia8JnA");

		cb.setOAuth2TokenType(token.getTokenType());
		cb.setOAuth2AccessToken(token.getAccessToken());

		//	And create the Twitter object!
		return new TwitterFactory(cb.build()).getInstance();

	}

	@Override
	public List<UserPostData> getUserPosts() throws UnsupportedQueryException, LimitReachedException, Exception {
		
		List<UserPostData> demo_data = new ArrayList<UserPostData>();
		demo_data.add(new UserPostData(new Date(System.currentTimeMillis()-122),1,"name","This is my text"));
		demo_data.add(new UserPostData(new Date(System.currentTimeMillis()-123),1,"name","This is my text"));
		demo_data.add(new UserPostData(new Date(System.currentTimeMillis()-12),1,"name","This is my text"));
		demo_data.add(new UserPostData(new Date(System.currentTimeMillis()-4),1,"name","This is my text"));
		demo_data.add(new UserPostData(new Date(System.currentTimeMillis()-6),1,"name","This is my text"));
		demo_data.add(new UserPostData(new Date(System.currentTimeMillis()-12),1,"name","This is my text"));
		demo_data.add(new UserPostData(new Date(System.currentTimeMillis()-123),1,"name","This is my text"));
		demo_data.add(new UserPostData(new Date(System.currentTimeMillis()-43),1,"name","This is my text"));
		
		
		boolean a = true;
		if(a) return demo_data;
		
		
		
		
		
		
		
		Twitter twitter = getTwitter();
		
		List<UserPostData> userPostDatafinal = new ArrayList<UserPostData>();

		// Check if the SocialQuery is a Twitter Query
		/*
		if (!(query.getClass() == TwitterQuery.class)) {
			throw new UnsupportedQueryException("The provided SocialQuery instance is of type " + query.getClass() +
					" instead of " + TwitterQuery.class.getName());
		}
		*/

		Map<String, RateLimitStatus> rateLimitStatus = twitter.getRateLimitStatus("search");

		//	This finds the rate limit specifically for doing the search API call we use in this program
		RateLimitStatus searchTweetsRateLimit = rateLimitStatus.get("/search/tweets");

		System.out.printf("You have %d calls remaining out of %d, Limit resets in %d seconds\n",
				searchTweetsRateLimit.getRemaining(),
				searchTweetsRateLimit.getLimit(),
				searchTweetsRateLimit.getSecondsUntilReset());


		for (int queryNumber = 0; queryNumber < MAX_QUERIES; queryNumber++) {
			System.out.printf("\n\n!!! Starting loop %d\n\n", queryNumber);

			//	Do we need to delay because we've already hit our rate limits?
			if (searchTweetsRateLimit.getRemaining() == 0) {
				//	Yes we do, unfortunately ...
				System.out.printf("!!! Sleeping for %d seconds due to rate limits\n", searchTweetsRateLimit.getSecondsUntilReset());

				//	If you sleep exactly the number of seconds, you can make your query a bit too early
				//	and still get an error for exceeding rate limitations
				//
				// 	Adding two seconds seems to do the trick. Sadly, even just adding one second still triggers a
				//	rate limit exception more often than not.  I have no idea why, and I know from a Comp Sci
				//	standpoint this is really bad, but just add in 2 seconds and go about your business.  Or else.
				Thread.sleep((searchTweetsRateLimit.getSecondsUntilReset() + 2) * 1000l);
			}
			Query q = new Query(search_query);			// Search for tweets that contains this term
			q.setCount(TWEETS_PER_QUERY);				// How many tweets, max, to retrieve
			q.setLang("en");							// English language tweets, please

			if (maxID != -1)
			{
				q.setMaxId(maxID - 1);
			}

			QueryResult r = twitter.search(q);

			if (r.getTweets().size() == 0)
			{
				break;
			}


			for (Status t : r.getTweets()) {
 //               totalTweets++;
                if (maxID == -1 || t.getId() < maxID)
                {
                    maxID = t.getId();
                }
                UserPostData userPostData = new UserPostData(t.getCreatedAt(), t.getId(), t.getUser().getScreenName(), cleanText(t.getText()));
                userPostDatafinal.add(userPostData);
                searchTweetsRateLimit = r.getRateLimitStatus();
            }


		}
		return userPostDatafinal;
	}

	@Override
	public String getProviderName() {
		return "Twitter Provider";
	}

	@Override
	public String getProviderFAIcon() {
		return "twitter";
	}

	@Override
	public Form getProviderConfigForm() {
		return config_form;
	}

	@Override
	public void configureProvider(Request req) throws Exception {

		if(Utils.hasRequestValueNotNull(req, "search_query")){
			String search_query = req.queryParams("search_query");
			
			if(search_query == null || search_query.trim().equals("")){
				throw new Exception("ERROR: Invalid search query.");
			}
			this.search_query = search_query;
			config_form.updateValue("search_query", search_query);
			
		}else{
			throw new Exception("ERROR: Missing parameter search_query");
		}
	}

	@Override
	public void setQuery(String query) {
		this.search_query = query;
	}

	@Override
	public String getQuery() {
		return this.search_query;
	}

	@Override
	public void start(SharedSpace shared) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

}

