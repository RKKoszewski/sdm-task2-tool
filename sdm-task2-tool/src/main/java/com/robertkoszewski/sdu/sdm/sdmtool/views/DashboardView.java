/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.views;

import com.robertkoszewski.sdu.sdm.sdmtool.Configuration;
import com.robertkoszewski.sdu.sdm.sdmtool.research.ResearchManager;
import com.robertkoszewski.utils.StringUtils;
import com.robertkoszewski.utils.SystemInfo;
import com.robertkoszewski.wui.Content;
import com.robertkoszewski.wui.SharedSpace;
import com.robertkoszewski.wui.widgets.*;

public class DashboardView{
	 
	private Content content = new Content();
	
	// Row 1
	LiquidRow row1;
	
	// Counters
	CountTile research_count;
	CountTile research_running_count;
	CountTile research_stop_count;
	CountTile ram_usage;
	CountTile os_version;
	CountTile total_ram_usage;
	
	public DashboardView(){
		// Counters
		LiquidRow row0 = new LiquidRow();
		CountTileRow count_tile_row = new CountTileRow();
		row0.addElement(12, count_tile_row);
		
		// General Stats
		research_count = new CountTile("search", "Total Researches", "0");
		count_tile_row.addElement(research_count);
		research_running_count = new CountTile("play-circle","Running Researches", "0");
		count_tile_row.addElement(research_running_count);
		research_stop_count = new CountTile("stop-circle","Stopped Researches", "0");
		count_tile_row.addElement(research_stop_count);
		
		// Sys Info
		os_version = new CountTile("OS Version", "%");
		count_tile_row.addElement(os_version);
		ram_usage = new CountTile("RAM Usage","");
		count_tile_row.addElement(ram_usage);
		total_ram_usage = new CountTile("Total RAM Alocated","");
		count_tile_row.addElement(total_ram_usage);

		// Mini Readme
		row1 = new LiquidRow();
		Panel panel_readme = new Panel("Readme");
		panel_readme.addElement(new Label("Welcome to the Social Dataming Tool version "+Configuration.app_version+"."));
		panel_readme.addElement(new Label("If you are using the Social Datamining Tool for the first time it is recommended to have a look at the Manual which you "+
		"can find in the sidebar menu."));
		panel_readme.addElement(new Label("PLEASE NOTE: Remember to setup your Twitter account in the Setting menu to be able to use the Twitter services for research."));
		row1.addElement(4, panel_readme);

		content.setTitle("Dashboard");
		content.addElement(row0);
		content.addElement(row1);
	}
	
	private boolean table_added = false; 
	
	public void updateData(SharedSpace shared, SystemInfo sysinfo){
		os_version.setData(sysinfo.OSname());
		ram_usage.setData(StringUtils.readableFileSize(sysinfo.usedMem()));
		total_ram_usage.setData(StringUtils.readableFileSize(sysinfo.totalMem()));
		
		ResearchManager research_manager = shared.getObject("research_manager", ResearchManager.class);
		if(research_manager != null){
			
			int total_r = research_manager.getResearchCount();
			int run_r = research_manager.getRunningResearchCount();
			int stop_r = total_r - run_r;
			
			research_count.setData(total_r+"");
			research_running_count.setData(run_r+"");
			research_stop_count.setData(stop_r+"");
			
		}
		
		if(!table_added){
			Table research_table = shared.getObject("research_list", Table.class);
			if(research_table != null){
				row1.addElement(8, new Panel("Research List", research_table));
				table_added = true;
			}
		}
	}
	
	/**
	 * Return Page Content
	 * @param shared 
	 * @return
	 */
	public Content getContent() {
		return content;
	}

}
