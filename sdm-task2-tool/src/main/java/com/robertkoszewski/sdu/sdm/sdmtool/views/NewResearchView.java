/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.views;

import com.robertkoszewski.wui.Content;
import com.robertkoszewski.wui.widgets.*;

/**
 * New Research Label
 * @author Robert Koszewski
 */
public class NewResearchView {
	
	private Content content;
	private Label info_label;
	
	public NewResearchView(){
		// Info
		info_label = new Label("In this section you can create a new research.");
		Panel info_panel = new Panel("Information");
		info_panel.addElement(info_label);
		
		// Form
		Form new_form = new Form("newresearch");
		new_form.addTextInput("research_id", "Research ID", null, TextInput.Type.text, true);
		new_form.addTextInput("research_name", "Research Name", null, TextInput.Type.text, true);
		new_form.addSeparator("separator");
		new_form.addSubmitButton("submit_newresearch", "Submit");
		Panel new_form_panel = new Panel("Create new research project");
		new_form_panel.addElement(new_form);
		
		// Liquid Row
		LiquidRow row_1 = new LiquidRow();
		row_1.addElement(3, info_panel);
		row_1.addElement(9, new_form_panel);
		
		// Content
		content = new Content();
		content.setTitle("Start New Research");
		content.addElement(row_1);
	}

	public void changeLabel(String text){
		info_label.setText(text);
	}

	public Content getContent(){
		return this.content;
	}
	
}
