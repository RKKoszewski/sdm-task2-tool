/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.views;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import com.robertkoszewski.sdu.sdm.sdmtool.research.Research;
import com.robertkoszewski.sdu.sdm.sdmtool.research.ResearchManager;
import com.robertkoszewski.wui.*;
import com.robertkoszewski.wui.widgets.*;

/**
 * Research List View
 * @author Robert Koszewski
 *
 */
public class ResearchListView {
	
	private Content content;
	private Table research_list;

	public ResearchListView(SharedSpace shared){
		// Research Table
		research_list = new Table("ID","Name","Status","Actions");
		research_list.setColumnWidths("10%","60%", "10%", "20%");
		research_list.setEmptyText("There are currently no researches to list. Please create a new research.");
		
		// Research Panel
		Panel research_list_panel = new Panel("List of Current Researches");
		research_list_panel.addElement(research_list);
		
		// Add Shared Object
		shared.addObject("research_list", research_list);
		
		// Content
		content = new Content();
		content.setTitle("Researches");
		content.addElement(research_list_panel);
	}
	
	public void updateResearchList(ResearchManager research_manager){
		research_list.cleanRows();
		Set<Entry<String, Research>> list = research_manager.getResearchList();
		Iterator<Entry<String, Research>> list_iterator = list.iterator();
		while(list_iterator.hasNext()){
			Entry<String, Research> element = list_iterator.next();
			Research r = element.getValue();
			research_list.addRow(new Label(element.getKey()), new Label(r.getResearchDescription()), new Label(r.isRunning()?"Running":"Stopped"), new Label("View Research","/research/"+element.getKey()));
		}
	}
	
	public Content getContent(){
		return content;
	}
}
