/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.views;

import com.robertkoszewski.wui.Content;
import com.robertkoszewski.wui.widgets.*;

/**
 * Research Not Found View
 * @author Robert Koszewski
 */
public class ResearchNotFoundView {

	private Content content;

	public ResearchNotFoundView(){
		// Research Table
		Label research_not_found_label = new Label("The research you are trying to access does not exist or has been removed.");
		
		// Research Panel
		Panel research_not_found_panel = new Panel("ERROR: Research not found");
		research_not_found_panel.addElement(research_not_found_label);
		
		// Content
		content = new Content();
		content.setTitle("ERROR: Research not found");
		content.addElement(research_not_found_panel);
	}
	
	public Content getContent(){
		return content;
	}
}
