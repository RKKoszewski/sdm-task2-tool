/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.views;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import com.robertkoszewski.sdu.sdm.sdmtool.dataprocessors.DataProcessor;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.SocialProvider;
import com.robertkoszewski.wui.Content;
import com.robertkoszewski.wui.widgets.*;

public class ResearchView {
	
	private Content content;
	private Label research_id;
	private Label research_desc;
	
	// Provider Tables
	private Table providers_table;
	private Table active_providers_table;
	private Table data_processors_table;
	
	// Research Settings
	private LiquidRow row1;
	private LiquidRow row_research_settings;
	private LiquidRow row_research_data;
	private Panel control_panel_start;
	private Panel control_panel_stop;

	public ResearchView(String research_id_str, String research_desc_str){
		// Row 1
		row1 = new LiquidRow();

		// Research Information
		Panel info_panel = new Panel("Research Info");
		research_id = new Label("Research ID: "+research_id_str);
		research_desc = new Label("Research Description: "+research_desc_str);
		info_panel.addElement(research_id);
		info_panel.addElement(research_desc);
		row1.addElement(8, info_panel);
		
		// Control Panel - Start
		control_panel_start = new Panel("Control Panel");
		LiquidRow control_row = new LiquidRow();
		Form control_form_start = new Form("controlStart");
		control_form_start.addSubmitButton("controlStart", "Start", false);
		control_row.addElement(6, control_form_start);
		
		Form control_form_remove = new Form("controlRemove");
		control_form_remove.addSubmitButton("controlRemove", "Remove", false);
		control_row.addElement(6, control_form_remove);
		
		control_panel_start.addElement(control_row);
		
		
		// Control Panel - Stop
		control_panel_stop = new Panel("Control Panel");
		Form control_form_stop = new Form("controlStop");
		control_form_stop.addSubmitButton("controlStop", "Stop", false);
		control_panel_stop.addElement(control_form_stop);
		
		
		
		
		row1.addElement(4, control_panel_start);

		// Content
		content = new Content();
		content.setTitle("Research");
		content.addElement(row1);
		
		// Research Settings Row
		row_research_settings = new LiquidRow();
		
		// Providers
		LiquidRow providers_row = new LiquidRow();
		
		// Providers: Available
		Panel providers_panel = new Panel("Available Providers");
		providers_table = new Table("", "Provider","Action");
		providers_table.setColumnWidths("1%", "95%", "4%");
		providers_table.setEmptyText("We're sorry but there are currently no providers available.");
		providers_panel.addElement(providers_table);
		providers_row.addElement(12, providers_panel);
		
		// Providers: Enabled
		Panel en_providers_panel = new Panel("Enabled Providers");
		active_providers_table = new Table("", "Provider", "Settings", "Action");
		active_providers_table.setColumnWidths("1%", "10%", "85%", "4%");
		active_providers_table.setEmptyText("We're sorry but there are currently no active providers available.");
		en_providers_panel.addElement(active_providers_table);
		providers_row.addElement(12, en_providers_panel);

		row_research_settings.addElement(6, providers_row);
		
		// Data Processors
		Panel data_processors_panel = new Panel("Data Processors");
		data_processors_table = new Table("", "Processor","Action");
		data_processors_table.setColumnWidths("1%","95%","5%");
		data_processors_table.setEmptyText("We're sorry but there are currently no providers available.");
		data_processors_panel.addElement(data_processors_table);
		row_research_settings.addElement(6, data_processors_panel);
		
		// Research Data Row
		row_research_data = new LiquidRow();
		
		// Content
		content = new Content();
		content.setTitle("Research");
		content.addElement(row1);
		content.addElement(row_research_settings);
	}
	
	public void setResearchID(String research_id){
		this.research_id.setText("Research ID: "+research_id);
	}
	
	public void setResearchDescription(String research_desc){
		this.research_desc.setText("Research Description: "+research_desc);
	}
	
	public void setProviderList(SocialProvider[] provider_list){
		providers_table.cleanRows();
		for(SocialProvider prov: provider_list){
			Form submit_button = new Form("addProvider");
			Label provider_icon = new Label("");
			provider_icon.setFAIcon(prov.getProviderFAIcon());
			provider_icon.setFAIconSize(30);
			submit_button.addSubmitButton("submit", "Add", false, Button.Color.INFO);
			submit_button.appendTextInput("providerID", null, prov.getClass().getName(), TextInput.Type.hidden, false);
			providers_table.addRow(provider_icon, new Label(prov.getProviderName()), submit_button);
		}
	}
	
	public void setRunningState(boolean running_state, DataProcessor[] data_processors){
		if(running_state){
			// Start Research
			content.removeElement(row_research_settings);
			row1.removeElement(control_panel_start);
			row1.addElement(4, control_panel_stop);
			
			for(DataProcessor dp: data_processors){
				if(dp.isEnabled())
					row_research_data.addElement(12, dp.getVisualData());
			}
			
			content.addElement(row_research_data);
			
		}else{
			// Stop Research
			row_research_data.clearElements();
			content.addElement(row_research_settings);
			row1.removeElement(control_panel_stop);
			row1.addElement(4, control_panel_start);
			control_panel_start.updateTimestamp();
			row1.updateTimestamp();
			content.updateTimestamp();
		}
	}
	
	
	/**
	 * Set Active Provider List
	 * @param active_providers
	 */
	public void setActiveProviderList(Map<UUID, SocialProvider> active_providers){
		active_providers_table.cleanRows();
		Iterator<Entry<UUID, SocialProvider>> providers_iterator = active_providers.entrySet().iterator();
		while(providers_iterator.hasNext()){
			Entry<UUID, SocialProvider> prov = providers_iterator.next();
			Label provider_icon = new Label("");
			provider_icon.setFAIcon(prov.getValue().getProviderFAIcon());
			provider_icon.setFAIconSize(30);
			Form remove_button = new Form("removeProvider");
			remove_button.addSubmitButton("submit", "Remove", false, Button.Color.DANGER);
			remove_button.appendTextInput("providerID", null, prov.getKey().toString(), TextInput.Type.hidden, false);
			Form provider_form = prov.getValue().getProviderConfigForm();
			active_providers_table.addRow(provider_icon, new Label(prov.getValue().getProviderName()), provider_form, remove_button);
		}
	}
	
	public void setDataProcessorList(DataProcessor[] processor_list){
		data_processors_table.cleanRows();
		for(DataProcessor processor: processor_list){
			Form submit_button = new Form("enableProcessor");
			Label provider_icon = new Label("");
			provider_icon.setFAIcon(processor.getFAIcon());
			provider_icon.setFAIconSize(30);
			if(processor.isEnabled()){
				submit_button.addSubmitButton("submit", "Enabled", false, Button.Color.SUCCESS);
				submit_button.appendTextInput("action", null, "disable", TextInput.Type.hidden, false);
			}else{
				submit_button.addSubmitButton("submit", "Disabled", false, Button.Color.DANGER);
				submit_button.appendTextInput("action", null, "enable", TextInput.Type.hidden, false);
			}
			submit_button.appendTextInput("providerID", null, processor.getClass().getName(), TextInput.Type.hidden, false);
			
			data_processors_table.addRow(provider_icon, new Label(processor.getName()), submit_button);
		}
	}
	
	public Content getContent(){
		return content;
	}

	@SuppressWarnings("deprecation")
	public void updateActiveProviderTable() {
		active_providers_table.forceUpdateTable();
	}

}
