/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.sdu.sdm.sdmtool.views;

import com.robertkoszewski.wui.Content;
import com.robertkoszewski.wui.widgets.*;

/**
 * Research Not Found View
 * @author Robert Koszewski
 */
public class SettingsView {

	private Content content;
	private Panel twitter_settings;
	private Label twitter_auth_status;
	private Form start_auth;
	private Form pin_auth;
	
	public SettingsView(){
		// Research Table
		twitter_auth_status = new Label("Account Status: Not authenticated");
		
		// Research Panel
		twitter_settings = new Panel("Twitter Provider Settings");
		twitter_settings.addElement(twitter_auth_status);
		
		// Content
		content = new Content();
		content.setTitle("Settings");
		content.addElement(twitter_settings);
		
		// Start Auth
		start_auth = new Form("twitterStartAuth");
		start_auth.addSubmitButton("twitterStartAuth", "Authenticate", false);
		
		// Auth Pin
		pin_auth = new Form("twitterPinAuth");
		pin_auth.addTextInput("pin", "PIN", "", TextInput.Type.text, true);
		pin_auth.addSubmitButton("twitterStartAuth", "Link Account", false);
	}
	
	public void setTwitterAuthenticated(boolean isAuthenticated, String screenname){
		if(!isAuthenticated){
			twitter_auth_status.setText("Account Status: Not authenticated");
			twitter_settings.addElement(start_auth);
			
			
		}else{
			twitter_auth_status.setText("Account Status: Is Authenticated (@"+screenname+")");
			twitter_settings.removeElement(start_auth);
			twitter_settings.removeElement(pin_auth);
			content.updateTimestamp();
			
		}
	}
	
	
	
	public void enableTwitterPinMode(){
		content.updateTimestamp();
		pin_auth.updateTimestamp();
		twitter_settings.addElement(pin_auth);
	}
	
	public Content getContent(){
		return content;
	}
}
