/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.structs;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Cumulative List which allows to add elements and be emptied on retrival (Thread Safe)
 * @author Robert Koszewski
 *
 * @param <T>
 */
public class CumulativeList<T> {

	private ArrayList<T> array;
	private Lock lock;
	
	public CumulativeList(){
		this.array = new ArrayList<T>();
		this.lock = new ReentrantLock();
	}
	
	/**
	 * Add a new Element
	 * @param obj
	 */
	public void add(T obj){
		lock.lock();
			array.add(obj);
		lock.unlock();
	}
	
	/**
	 * Return a Snapshot (Without removing current elements)
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<T> getSnapshot(){
		lock.lock();
			List<T> clone = (List<T>) array.clone();
		lock.unlock();
		return clone;
	}
	
	/**
	 * Returns the Current List (Removing previous one)
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<T> getList(){
		lock.lock();
			List<T> clone = (List<T>) array.clone();
			array.clear();
		lock.unlock();
		return clone;
	}
}
