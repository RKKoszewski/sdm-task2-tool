/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.structs;

import java.util.Stack;

/**
 * Limited Stack Implementation
 * @author Robert Koszewski
 * @param <E>
 */
public class LimitedStack <E> extends Stack<E>{

	private static final long serialVersionUID = 8780869203280487279L;
	private int maxSize;
	private int offset = 0;
	
    public LimitedStack(int size){
        super();
        this.maxSize=size;
    }

    @Override
    public E push(E elt) {
        super.push(elt);
        while (this.size() > this.maxSize) {
            this.removeElementAt(this.size() - 1);
            offset++;
        }
        return null;
    }
    
    @Override
    public boolean add(E elt) {
        while (this.size() >= this.maxSize) {
            this.removeElementAt(0);
        }
        return super.add(elt);
    }
    
    public int getOffset(){
    	return offset;
    }
    
    public int getFullSize(){
    	return this.size()+offset;
    }
    
    
}
