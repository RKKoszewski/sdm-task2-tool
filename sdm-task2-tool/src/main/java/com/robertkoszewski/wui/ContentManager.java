/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.wui;

import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.after;
import static spark.Spark.staticFiles;
import spark.Request;
import spark.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import com.robertkoszewski.utils.ReflectionUtils;
import com.robertkoszewski.utils.SocketUtils;
import com.robertkoszewski.utils.ReflectionUtils.ClassMethod;


/**
 * Window Content Manager
 * @author Robert Koszewski
 *
 */
public class ContentManager {
	
	private WindowTemplate template;
	private ReflectionUtils reflection;
	private int server_port;
	private String[] scan_packages = new String[]{"com.robertkoszewski.sdu.sdm.sdmtool.controllers","sdmtoolext"};
	private SharedSpace shared_space;
	private Persistence persistence;
	
	/**
	 * Initialize Content Manager
	 * @param template
	 */
	public ContentManager(WindowTemplate template, String appid, int server_port){
		this.template = (WindowTemplate) template;
		this.reflection = new ReflectionUtils();
		this.shared_space = new SharedSpace();
		this.persistence = new Persistence(appid);
		this.server_port = server_port;
		this.initializeContent();
	}

	/**
	 * Initialize Content
	 */
	public void initializeContent(){
		// Select Server Port
		if(server_port == 0)
			server_port = SocketUtils.getOpenPort();
		port(server_port);

		// Static Files
		staticFiles.location("/web"); // Static files
		
		// WUI Elements
		get("/wui/elements", (req, res) -> WUIRenderer.getElements());
		get("/wui/element_controllers.js", (req, res) -> WUIRenderer.getElementControllers());
		
		// Controller Initializer Parameter Injection Map
		ReflectionUtils.ParameterMap icpm = new ReflectionUtils.ParameterMap();
		icpm.put(this.template);
		icpm.put(this.persistence);
		icpm.put(this.shared_space);
		
		// Initialize Controllers
		try {
			ArrayList<ClassMethod> method_list = reflection.findMethodsByAnnotation(InitializeController.class, scan_packages);
			Iterator<ClassMethod> iterator = method_list.iterator();
			while(iterator.hasNext()){
				ClassMethod cm = iterator.next();
				try {
					// Instantiate Class and Add to Cache (This may be commented out for a LazyLoad approach)
					reflection.instantiateClass(cm.class_name); 
					// Execute Initialize Method
					reflection.callMethodWithInjection(cm, icpm);
					
				} catch (Exception e) {
					e.printStackTrace();
				}	
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Initialize Component
		try {
			ArrayList<ClassMethod> method_list = reflection.findMethodsByAnnotation(Route.class, scan_packages);
			Iterator<ClassMethod> iterator = method_list.iterator();
			while(iterator.hasNext()){
				ClassMethod cm = iterator.next();
				try {
					Route route = (Route) reflection.getAnnotation(cm, Route.class);
					String url = route.url();
					
					// Instantiate Class and Add to Cache (This may be commented out for a LazyLoad approach)
					reflection.instantiateClass(cm.class_name); 
					// GET Request Redirects to Base Template
					get(url, (req, res) -> template.getTemplateHTML());
					// POST Request Handles Page Content
					post(url, (req, res) -> this.executeRequest(req, res, url, cm));
					
				} catch (Exception e) {
					e.printStackTrace();
				}	
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Enable GZIP
		after((request, response) -> {
			if(!response.body().equals("0")){ // Exclude NoOp response from GZIP compression because it generates bigger files
				response.header("Content-Encoding", "gzip");
			}
		});
	}
	
	/**
	 * Execute Request
	 * @param req
	 * @param res
	 * @param url
	 * @param class_name
	 * @param method_name
	 * @param annotation_class
	 * @throws Exception 
	 */
	private String executeRequest(Request req, Response res, String url, ClassMethod cm) throws Exception{
		// Injection Features
		UIControl uicontrol = new UIControl();
		
		// Build Injection Map
		ReflectionUtils.ParameterMap pm = new ReflectionUtils.ParameterMap();
		pm.put(req); // Request
		pm.put(res); // Response
		pm.put(url); // Current URL (String)
		pm.put(shared_space); // SharedSpace
		pm.put(persistence);
		pm.put(uicontrol);
		
		//ContentResponse
		
		try{
		
			// Call Method
			Object response = reflection.callMethodWithAnnotationUsingInjection(cm, Route.class, pm);
			
			// Check for time-stamp
			if(response instanceof Content){ // Content Response
				long timestamp = 0;
				String timestamp_query = req.queryParams("wui-timestamp");
				if(timestamp_query != null){
					try{
					timestamp = Long.parseLong(timestamp_query);
					}catch(Exception e){}
				}
				return template.generateResponse(req, res, (Content) response, uicontrol, timestamp); 
				
			}else if(response instanceof String){ // Raw String Response
				return (String) response; 
				
			}else{
				return null; // Null Response
			}
		
		}catch(Exception e){
			e.getCause().printStackTrace();
			// Show error message
			ContentResponse cr = new ContentResponse();
			Notification n = new Notification("ERROR: Internal Server Error (500)",
					"An error happened on the serve side: "+e.getCause().toString(), Notification.Type.ERROR);
			ArrayList<Notification> nl = new ArrayList<Notification>();
			nl.add(n);
			cr.wui_notifications = nl;
			return HTMLElementSerializer.getSerializer(0, 0, cr, null).toJson(cr);
		}
	} 
	
	/**
	 * Return the Server Port
	 * @return
	 */
	public int getServerPort(){
		return this.server_port;
	}
	
	// TODO: Implement this (Not crucial for now)
	// public void destroy(){};
}
