/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.wui;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import com.google.gson.*;
import com.google.gson.annotations.Expose;

/**
 * HTMLElement Serializer for Gson
 * @author Robert Koszewski
 */
public class HTMLElementSerializer implements JsonSerializer<Object>{
	
	//private int highest_timestamp = 0;
	private final long timestamp;
	ContentResponse response;
	SerializerStatus status;
	
	private final boolean timestamp_debug = false;
	
	HTMLElementSerializer(long timestamp, long dom_timestamp, ContentResponse response, SerializerStatus status){
		this.timestamp = timestamp;
		this.response = response;
		this.response.timestamp = dom_timestamp;
		this.status = status;
	}
	
	@Override
	public JsonElement serialize(Object src, Type typeOfSrc, JsonSerializationContext context) {
		
		// Element Data
		JsonObject element = new JsonObject();
		
		// WUI Element
		element.addProperty("wui.element", src.getClass().getSimpleName());

		// Unique Identifiable Element
		if(src instanceof UniqueElement){
			element.addProperty("wui.uid", ((UniqueElement) src).getElementUID());
		}
		
		// Set latest timestamp
		if(src instanceof TimestampedElement){
			long element_timestamp = ((TimestampedElement) src).getTimestamp();
			// System.out.println("Element: "+element_timestamp+" | Response: "+response.timestamp+" | Global: "+timestamp);
			//element.addProperty("timestamp", element_timestamp);
			if(response.timestamp < element_timestamp){
				response.timestamp = element_timestamp;
			}

			if(timestamp < element_timestamp && status != null){
				status.nodedata_update = true;
			}
			
			if(timestamp_debug)
				element.addProperty("wui.timestamp", element_timestamp);
		}

		// Get Field Data
		Field[] fieldList = src.getClass().getDeclaredFields();

        for (Field field : fieldList) {
        	
        	if(field.isAnnotationPresent(Expose.class)){	
        		try {
        			boolean accessible = field.isAccessible(); // Get Accessible Status
        			if(!accessible) field.setAccessible(true); // Set Accessible
        			
        			Object obj = field.get(src);
        			
        			if(obj instanceof HTMLElement){
        				// Parse all HTMLElements
        				element.add(field.getName(), context.serialize(field.get(src))); // Get Field Data
        				
        			}else{
        				// Only re-send all data if current element's time-stamp is greater than request time-stamp
        				if(timestamp == 0){ 
        					// Send all data (Including non-time-stamped elements)
        					element.add(field.getName(), context.serialize(field.get(src))); // Get Field Data
        					
        				}else{	
        					// Time filtered data (Only for time-stamped elements)
        					if(src instanceof TimestampedElement){
        						long element_timestamp = ((TimestampedElement) src).getTimestamp();

            					if(element_timestamp > timestamp){
            						element.add(field.getName(), context.serialize(field.get(src))); // Get Field Data
            						
            					}else{
            						element.addProperty("wui.nochange", true);
            						
            					}
            				}else{
            					element.add(field.getName(), context.serialize(field.get(src))); // Get Field Data
            					
            				}
        				}
        			}
        			
        			if(!accessible) field.setAccessible(false); // Set to Unaccessible state again
        			
				} catch (Exception e) {
					e.printStackTrace();
				}
        		
        	}else if(field.isAnnotationPresent(ExposeNode.class)){ // Expose Node
        		try{
	        		boolean accessible = field.isAccessible(); // Get Accessible Status
	    			if(!accessible) field.setAccessible(true); // Set Accessible
	    			element.add(field.getName(), context.serialize(field.get(src))); // Get Field Data
	    			if(!accessible) field.setAccessible(false); // Set to Unaccessible state again
	        	} catch (Exception e) {
					e.printStackTrace();
				}
        	}
        }

	    return element;
	}

	/**
	 * Get a Gson Serializer Instance
	 * @return
	 */
	public static Gson getSerializer(long timestamp, long dom_timestamp, ContentResponse response, SerializerStatus status){
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeHierarchyAdapter(HTMLElement.class, new HTMLElementSerializer(timestamp, dom_timestamp, response, status));
		gsonBuilder.registerTypeHierarchyAdapter(TimestampedElement.class, new HTMLElementSerializer(timestamp, dom_timestamp, response, status));
		return gsonBuilder.create();
	}
}
