/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.wui;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.robertkoszewski.sdu.sdm.sdmtool.dataprocessors.DataProcessor;
import com.robertkoszewski.sdu.sdm.sdmtool.research.Research;
import com.robertkoszewski.sdu.sdm.sdmtool.socialproviders.SocialProvider;
import com.robertkoszewski.utils.FileUtils;

/**
 * Persistence Class
 * @author Robert Koszewski
 *
 */
public class Persistence {
	
	//private final String app_id;
	//private final Genson serializer;
	private final Gson serializer;
	private final String working_dir;
	
	private static final String obj_file_extension = ".bo";

	public Persistence(String app_id){
		//this.app_id = app_id;
		
		// Initialize Serializer
		// GSON (Does not support Interface Classes)
		this.serializer = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
				.registerTypeAdapter(SocialProvider.class, new GsonInterfaceAdapter<SocialProvider>())
				.registerTypeAdapter(DataProcessor.class, new GsonInterfaceAdapter<DataProcessor>())
				.setPrettyPrinting()
				.create();

		// Genson (Handles Interfaces too)
		//this.serializer = new GensonBuilder().useClassMetadata(true).useRuntimeType(true).useFields(true, VisibilityFilter.PRIVATE).useMethods(false).create();

		// Setup Working Directory
        if(System.getProperty("user.home") != null && !System.getProperty("user.home").equals(""))
        	working_dir = System.getProperty("user.home") + File.separatorChar + "." + app_id + File.separatorChar;
        else
        	working_dir = "."+File.separatorChar;
        
        // Setup Environment
        File working_dir_obj = new File(working_dir);
        if(!working_dir_obj.exists()){
        	System.out.println("Setting up persistence folder in: " + working_dir);
        	working_dir_obj.mkdirs();
        }
	}
	
	/**
	 * Return the Persistence Path
	 * @return
	 */
	public String getPersistencePath(){
		return working_dir;
	}
	
	/**
	 * Persist a Object
	 * @param id
	 * @param obj
	 * @throws IOException 
	 */
	public void writeObject(String id, Object obj) throws IOException{
		File out_file = new File(working_dir + id + obj_file_extension);
		FileUtils.writeStringtoFile(out_file, serializer.toJson(obj)); // Gson
		//FileUtils.writeStringtoFile(out_file, serializer.serialize(obj)); // Genson
	}
	
	/**
	 * Read persisted Object
	 * @param id
	 * @param clazz
	 * @return
	 */
	public <T> T readObject(String id, Class<T> clazz) throws Exception{
		File in_file = new File(working_dir + id + obj_file_extension);
		return serializer.fromJson(FileUtils.readFiletoString(in_file), clazz); // Gson
//		System.out.println("DESERAILIZING: "+working_dir + id + obj_file_extension);
//		System.out.println("RESULT: "+serializer.deserialize(FileUtils.readFiletoString(in_file), clazz));
		//return serializer.deserialize(FileUtils.readFiletoString(in_file), clazz); // Genson
	}

	public Map<String, Research> readObject(String id, Type type) {
		File in_file = new File(working_dir + id + obj_file_extension);
		return serializer.fromJson(FileUtils.readFiletoString(in_file), type); // Gson
	}
	
}
