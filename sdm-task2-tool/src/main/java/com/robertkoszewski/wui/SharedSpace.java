/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.wui;

import java.util.HashMap;
import java.util.Map;

/**
 * Shared Space Class - Allows to share objects between different controllers
 * @author Robert Koszewski
 */
public class SharedSpace {
	
	private Map<String, Object> shared_space;
	
	public SharedSpace(){
		shared_space = new HashMap<String, Object>();
	}
	
	/**
	 * Returns a Object from the Shared Space
	 * @param object_id
	 * @return
	 */
	public Object getObject(String object_id){
		return shared_space.get(object_id);
	}
	
	/**
	 * Get Object with Automatic Cast
	 * @param object_id
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> T getObject(String object_id, Class<T> clazz){
		Object obj = shared_space.get(object_id);
		if(checkIfObjectIsInstance(obj, clazz)){
			return (T) obj;
		}
		return null;
	}
	
	/**
	 * Add object to the Shared Space
	 * @param object_id
	 * @param object
	 * @return
	 */
	public void addObject(String object_id, Object object){
		shared_space.put(object_id, object);
	}
	
	/**
	 * Remove a Object from the Shared Space
	 * @param object_id
	 */
	public void removeObject(String object_id){
		shared_space.remove(object_id);
	}
	
	/**
	 * Check if object is instance of
	 * @param obj
	 * @param clazz
	 * @return
	 */
	public static boolean checkIfObjectIsInstance(Object obj, Class<?> clazz){
		return obj != null && clazz.isInstance(obj);
	}
	
	/*
	public void removeObject(Object object){
		shared_space.remove(object);
	}
	*/

}
