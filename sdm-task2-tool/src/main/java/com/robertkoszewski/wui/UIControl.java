/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.wui;

import java.util.ArrayList;

/**
 * UI Control Class
 * @author Robert Koszewski
 */
public class UIControl {
	
	private boolean uicontrol_updated = false;
	private ArrayList<Notification> notifications;
	private String redirect_url;
	
	public UIControl(){
		notifications = new ArrayList<Notification>();
	}
	
	/**
	 * Show a Notification
	 * @param title
	 * @param message
	 * @param type
	 */
	public void addNotification(String title, String message, com.robertkoszewski.wui.Notification.Type type){
		notifications.add(new Notification(title, message, type));
		uicontrol_updated = true;
	}
	
	/**
	 * Get Number of Notifications
	 * @return
	 */
	public int getNotificationCount(){
		return notifications.size();
	}
	
	/**
	 * Get Array of all Notifications
	 * @return
	 */
	public ArrayList<Notification> getNotificationList(){
		return notifications;
	}
	
	/**
	 * Return if UI Control has been updated
	 * @return
	 */
	public boolean isUIControlUpdated(){
		return uicontrol_updated;
	}
	
	/**
	 * Set a URL where the user will be redirected
	 * @param redirect_url
	 */
	public void setRedirectURL(String redirect_url){
		this.redirect_url = redirect_url;
		uicontrol_updated = true;
	}
	
	/**
	 * Get the current redirect URL
	 * @return
	 */
	public String getRedirectURL(){
		return this.redirect_url;
	}
}
