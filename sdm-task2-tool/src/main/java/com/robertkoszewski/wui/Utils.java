/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.wui;

import spark.Request;

/**
 * Utilities to make life easier
 * @author Robert Koszewski
 */
public class Utils {
	
	/**
	 * Check if Request values are Not NULL
	 * @param req
	 * @param value
	 * @return
	 */
	public static boolean hasRequestValueNotNull(Request req, String... values){
		for(String value: values)
			if(req.queryParams(value) == null)
				return false;
		return true;
	}
	
	/**
	 * Check if Request value equals the string
	 * @param req
	 * @param value_id
	 * @param value_content
	 * @return
	 */
	public static boolean hasRequestValueEquals(Request req, String value_id, String value_content){
		if(req.queryParams(value_id) != null && req.queryParams(value_id).equals(value_content))
			return true;
		else
			return false;
	}
	
	/**
	 * Check if Data ID equals
	 * @param req
	 * @param data_id
	 * @return
	 */
	public static boolean hasDataID(Request req, String data_id){
		return hasRequestValueEquals(req, "wui.dataid", data_id);
	}
}
