/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.wui;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Stream;
import com.google.gson.JsonObject;
import com.robertkoszewski.utils.StringUtils;

/**
 * WUI Renderer Tools
 * @author Robert Koszewski
 */
public class WUIRenderer {
	
	private static String elements_json = null;
	private static String elementcontrollers_js = null;
	
	/**
	 * 
	 * @return
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	public static String getElements(){
		
		if( elements_json == null ){
			
			try{
			
			// Generate Elements_JSON
			JsonObject elements_json_obj = new JsonObject();
			
			// Check Default Elements
			URI uri = WUIRenderer.class.getResource("/web/elements").toURI();
	        Path myPath;
	        if (uri.getScheme().equals("jar")) {
	        	FileSystem fileSystem;
	        	try{
	        		fileSystem = FileSystems.newFileSystem(uri, Collections.<String, Object>emptyMap());
	        	}catch(Exception e){
	        		fileSystem = FileSystems.getFileSystem(uri);
	        	}
	            myPath = fileSystem.getPath("/web/elements");
	        } else {
	            myPath = Paths.get(uri);
	        }
	        
	        Stream<Path> walk = Files.walk(myPath, 1);
	        for (Iterator<Path> it = walk.iterator(); it.hasNext();){
	        	Path path = it.next();
	        	
	        	// Template Files
	        	if(path.getFileName().toString().toLowerCase().endsWith(".html") 
	        	|| path.getFileName().toString().toLowerCase().endsWith(".htm")){
//	        		System.out.println("Adding WUI Widget: " + path.getFileName());
	        		//StringUtils
	        		String element = StringUtils.removeExtensionFromFilename(path.getFileName().toString());
	        		String source = new String(Files.readAllBytes(path));
	        		elements_json_obj.addProperty(element, source);
	        	}
	        	
	        	// TODO: Controller Files as .js files
	        }
	        walk.close();
	        
	        // Cache Result
	        elements_json = elements_json_obj.toString();
	        
			}catch(Exception e){
				System.err.println("ERROR: Elements not found.");
				e.printStackTrace();
				elements_json = "ERROR finding elements.";
			}
		}

		// Return cached Elements_JSON
		return elements_json; 
	}
	
	/**
	 * Return Element Controllers (Javascript)
	 * @return
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	public static String getElementControllers() throws URISyntaxException, IOException{
		
		if( elementcontrollers_js == null ){
			
			// Generate Elements_JSON
			String source = "";
			
			// Check Default Elements
			URI uri = WUIRenderer.class.getResource("/web/elements").toURI();
	        Path myPath;
	        if (uri.getScheme().equals("jar")) {
	            FileSystem fileSystem = FileSystems.newFileSystem(uri, Collections.<String, Object>emptyMap());
	            myPath = fileSystem.getPath("/web/elements");
	        } else {
	            myPath = Paths.get(uri);
	        }
	        
	        Stream<Path> walk = Files.walk(myPath, 1);
	        for (Iterator<Path> it = walk.iterator(); it.hasNext();){
	        	Path path = it.next();
	        	
	        	// Template Files
	        	if(path.getFileName().toString().toLowerCase().endsWith(".js")){
//	        		System.out.println("Adding WUI Widget Controller: " + path.getFileName());
	        		source += new String(Files.readAllBytes(path));
	        	}
	        	
	        }
	        walk.close();
	        
	        // Cache Result
	        elementcontrollers_js = source;
		}

		// Return cached Elements_JSON
		return elementcontrollers_js; 
	}
}
