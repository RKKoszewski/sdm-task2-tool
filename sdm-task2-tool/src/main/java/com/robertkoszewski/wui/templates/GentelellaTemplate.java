/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.wui.templates;

import java.util.*;
import com.robertkoszewski.utils.FileUtils;
import com.robertkoszewski.wui.*;

import spark.Request;
import spark.Response;

/**
 * Gentelella Based Template
 * @author Robert Koszewski
 */
public class GentelellaTemplate implements WindowTemplate{

	private TreeMap<Integer, Vector<MenuElementIDPair>> menu = new TreeMap<Integer, Vector<MenuElementIDPair>>();
	private String app_name;
	
	@Override
	/**
	 * Add Menu Element
	 */
	public void addMenuElement(String id, int weight_index, MenuElement element){
		
		Vector<MenuElementIDPair> prev = menu.get(weight_index);
		if(prev != null){
			prev.add(new MenuElementIDPair(id, element));
			
		}else{
			Vector<MenuElementIDPair> vector = new Vector<MenuElementIDPair>();
			vector.addElement(new MenuElementIDPair(id, element));
			menu.put(weight_index, vector);
			
		}
	}
	
	@Override
	/**
	 * Get Menu Element
	 */
	public MenuElement getMenuElement(String id) {
		Iterator<Vector<MenuElementIDPair>> i = menu.values().iterator();
		while(i.hasNext()){
			Vector<MenuElementIDPair> v = i.next();
			Iterator<MenuElementIDPair> vi = v.iterator();
			while(vi.hasNext()){
				MenuElementIDPair e = vi.next();
				if(e.id == id)
					return e.element;
			}
		}
		return null;
	}
	
	/**
	 * Get Sorted Menu Elements
	 * @return
	 */
	private ArrayList<MenuElement> getSortedMenuElements(){
		ArrayList<MenuElement> a = new ArrayList<MenuElement>();
		
		Iterator<Vector<MenuElementIDPair>> i = menu.values().iterator();
		while(i.hasNext()){
			Vector<MenuElementIDPair> v = i.next();
			Iterator<MenuElementIDPair> vi = v.iterator();
			while(vi.hasNext()){
				a.add(vi.next().element);
			}
		}
		
		return a;
	}
	
	@Override
	/**
	 * Set Title
	 */
	public void setAppName(String app_name){
		this.app_name = app_name;
	}
	
	@Override
	/**
	 * Get Title
	 */
	public String getAppName(){
		return this.app_name;
	}

	@Override
	/**
	 * Get Template HTML
	 */
	public String getTemplateHTML() {
		try {
			return FileUtils.readFileInJARToString("/web/templates/gentelella/base.html");
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR: Could not initialize base template";
		}
	}
	
	@Override
	/**
	 * Generate Response
	 */
	public String generateResponse(Request req, Response res, Content content, UIControl uicontrol, long timestamp){
		// Content Response
		ContentResponse response = new ContentResponse();
		response.app_name = this.app_name;
		response.title = content.getTitle();
		response.body = content.getContent();
		response.menu = getSortedMenuElements();

		// UI Control Related
		if(uicontrol.getNotificationCount() != 0)
			response.wui_notifications = uicontrol.getNotificationList();
		response.wui_redirect = uicontrol.getRedirectURL();
		
		boolean content_update = false;
		if(content.getTimestamp() > timestamp){
			content_update = true;
			
		}
		
		// Generating the Response
		SerializerStatus status = new SerializerStatus();
		String json_response = HTMLElementSerializer.getSerializer(timestamp, content.getTimestamp(), response, status).toJson(response);
		if(status.nodedata_update || uicontrol.isUIControlUpdated() || content_update){
			return json_response;
		}else{
			return "0"; // Smallest Valid JSON
		}
	}

	/**
	 * Menu ElementID Pair
	 */
	public class MenuElementIDPair {
		
		public final String id;
		public final MenuElement element;
		
		public MenuElementIDPair(String id, MenuElement element){
			this.id = id;
			this.element = element;
		}
	}
}
