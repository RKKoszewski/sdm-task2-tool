/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.wui.widgets;

import com.google.gson.annotations.Expose;
import com.robertkoszewski.wui.DynamicElement;
import com.robertkoszewski.wui.HTMLElement;

/**
 * Count Tile
 * @author Robert Koszewski
 */
public class CountTile extends DynamicElement implements HTMLElement{

	@Expose
	private String faicon;
	@Expose
	private String title;
	@Expose
	private String data;
	
	public CountTile(String faicon, String title, String data){
		this(title, data);
		this.faicon = faicon;
	}
	
	public CountTile(String title, String data){
		this.title = title;
		this.data = data;
	}
	
	public void setTitle(String title){
		this.title = title;
		updateTimestamp();
	}
	
	public void setData(String data){
		this.data = data;
		updateTimestamp();
	}
}
