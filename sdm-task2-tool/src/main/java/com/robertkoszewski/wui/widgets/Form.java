/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.wui.widgets;

import java.util.Iterator;
import java.util.Vector;

import org.apache.commons.lang.ArrayUtils;

import com.google.gson.annotations.Expose;
import com.robertkoszewski.wui.DynamicElement;
import com.robertkoszewski.wui.ExposeNode;
import com.robertkoszewski.wui.HTMLElement;
import com.robertkoszewski.wui.TimestampedElement;

public class Form extends DynamicElement implements HTMLElement{
	
	@Expose
	private String id;
	@ExposeNode
	private Boolean force_vertical;
	@ExposeNode
	private Vector<ElementLabelID> form_elements;
	
	public Form(String id){
		this.id = id;
		this.form_elements = new Vector<ElementLabelID>();
	}
	
	public void addTextInput(String id, String label, String value, TextInput.Type type, boolean required){
		form_elements.addElement(new ElementLabelID(id, label, new HTMLElement[]{new TextInput(id, value, type, required)}));
	}
	
	public void appendTextInput(String id, String label, String value, TextInput.Type type, boolean required){
		ElementLabelID elem = form_elements.get(form_elements.size()-1); // TODO: This is unsafe. Fix it.
		elem.appendElement(new TextInput(id, value, type, required));
	}
	
	public void addOptionRadio(String id, String label){}
	
	public void addOptionCheckbox(String id, String label){}
	
	public void addSeparator(String id){
		form_elements.addElement(new ElementLabelID(id, null, new HTMLElement[]{new Separator(Separator.Type.horizontal)}));
	}
	
	public void addSubmitButton(String id, String label){
		addSubmitButton(id, label, true);
	}
	
	public void addSubmitButton(String id, String label, boolean with_space){
		form_elements.addElement(new ElementLabelID(id, (with_space?" ":null), new HTMLElement[]{new Button(id, label, Button.Type.SUBMIT)}));
	}
	
	public void addSubmitButton(String id, String label, boolean with_space, Button.Color color){
		form_elements.addElement(new ElementLabelID(id, (with_space?" ":null), new HTMLElement[]{new Button(id, label, Button.Type.SUBMIT, color)}));
	}
	
	public void appendSubmitButton(String id, String label){
		ElementLabelID elem = form_elements.get(form_elements.size()-1); // TODO: This is unsafe. Fix it.
		elem.appendElement(new Button(id, label, Button.Type.SUBMIT));
	}
	
	public void forceVerticalLayout(boolean force_vertical){
		this.force_vertical = (force_vertical?true:null);
	}
	
	public void updateValue(String id, String value){
		Iterator<ElementLabelID> it = form_elements.iterator();
		while(it.hasNext()){
			ElementLabelID el = it.next();
	
			for(HTMLElement e: el.element){
				if(e instanceof TextInput){
					if(((TextInput) e).getID().equals(id)){
						((TextInput) e).setValue(value);
					}
				}
			}
			// TODO: Finish this
		}
		
		updateTimestamp();
	}
	
	@Override
	public void updateTimestamp(){
		super.updateTimestamp();
		Iterator<ElementLabelID> it = form_elements.iterator();
		while(it.hasNext()){
			ElementLabelID el = it.next();
	
			for(HTMLElement e: el.element){
				if(e instanceof TimestampedElement){
					((TimestampedElement) e).updateTimestamp();
				}
			}
		}
	}
	
	/**
	 * Element Label ID
	 */
	private static class ElementLabelID{
		
		@Expose final String id;
		@Expose final String label;
		@ExposeNode HTMLElement[] element;
		
		ElementLabelID(String id, String label, HTMLElement[] element){
			this.id = id;
			this.label = label;
			this.element = element;
		}
		
		public void appendElement(HTMLElement node){
			this.element = (HTMLElement[]) ArrayUtils.addAll(this.element, new HTMLElement[]{node});
		}
	}
}
