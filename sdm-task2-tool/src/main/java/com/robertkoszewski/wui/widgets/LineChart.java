/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.wui.widgets;

import java.awt.Color;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import com.google.gson.annotations.Expose;
import com.robertkoszewski.wui.DynamicElement;
import com.robertkoszewski.wui.HTMLElement;

/**
 * Line Chart
 * @author Robert Koszewski
 */
public class LineChart extends DynamicElement implements HTMLElement{
	
	@Expose
	private String[] labels;
	@Expose
	private Collection<LineChartDataset> dataset;
	private Map<String, LineChartDataset> dataset_map;

	public LineChart(){
		dataset_map = new HashMap<String, LineChartDataset>();
	}
	
	/**
	 * Set Labels
	 * @param labels
	 */
	public void setLabels(String... labels){
		this.labels = labels;
		updateTimestamp();
	}
	
	/**
	 * Add Dataset
	 * @param id
	 * @param dataset
	 */
	public void addDataset(String id, LineChartDataset data){
		dataset_map.put(id, data);
		dataset = dataset_map.values();
		updateTimestamp();
	}
	
	/**
	 * Add Dataset
	 * @param id
	 * @param label
	 * @param data
	 */
	public void addDataset(String id, String label, int[] data){
		addDataset(id, new LineChartDataset(label, data));
		updateTimestamp();
	}
	
	/**
	 * Add Dataset
	 * @param id
	 * @param label
	 * @param data
	 */
	public void addDataset(String id, String label, int[] data, Color color){
		addDataset(id, new LineChartDataset(label, data, color));
		updateTimestamp();
	}
	
	/**
	 * Get Labels
	 * @return
	 */
	public String[] getLabels(){
		return this.labels;
	}
	
	/**
	 * Line Chart Dataset
	 * @author Robert Koszewski
	 *
	 */
	public class LineChartDataset{

		@Expose
		private String label;
		@Expose
		private int[] data;
		
		@Expose
		private String backgroundColor;
		@Expose
		private String borderColor;
		@Expose
		private String pointBorderColor;
		//@Expose
		//private String pointBackgroundColor = "#fff";
		//@Expose
		//private String pointHoverBorderColor = "#fff";
		//@Expose
		//private String pointBorderWidth = "1";
		
		public LineChartDataset(String label, int[] data){
			this.label = label;
			this.data = data;
		}
		
		public LineChartDataset(String label, int[] data, Color color){
			this.label = label;
			this.data = data;
			// Set Colors
			this.backgroundColor = "rgba("+color.getRed()+", "+color.getGreen()+", "+color.getBlue()+", 0.2)";
			this.pointBorderColor = this.borderColor = "rgba("+color.getRed()+", "+color.getGreen()+", "+color.getBlue()+", 1)";
		}
	}
}
