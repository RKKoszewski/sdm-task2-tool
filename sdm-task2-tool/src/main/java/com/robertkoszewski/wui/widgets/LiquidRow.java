/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.wui.widgets;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import com.google.gson.annotations.Expose;
import com.robertkoszewski.wui.DynamicElement;
import com.robertkoszewski.wui.ExposeNode;
import com.robertkoszewski.wui.HTMLElement;
import com.robertkoszewski.wui.TimestampedElement;

/**
 * Add a Liquid Row
 * @author Robert Koszewski
 */
public class LiquidRow extends DynamicElement implements HTMLElement {
	
	@ExposeNode
	private Vector<LiquidRowElement> row_elements;
	private int sum = 0;

	public LiquidRow(){
		row_elements = new Vector<LiquidRowElement>();
	}
	
	/**
	 * Add Element to Row
	 * @param index
	 * @param element
	 */
	public void addElement(int col_width, HTMLElement element){
		int width = col_width;
		if(sum + col_width > 12){
			if(sum != 12){
				width = 12 - sum; // Just take up the space that is left
			}else{
				// TODO: Deal with the exception in some better way
				try {
					throw new Exception("Size goes beyond 12. Please create a new row.");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		row_elements.addElement(new LiquidRowElement(/*col_start,*/ width, element));
		
		if(element instanceof TimestampedElement){
			((TimestampedElement)element).updateTimestamp();
		}

		updateTimestamp();
	}
	
	/**
	 * Remove an Element
	 * @param element
	 */
	public void removeElement(HTMLElement element){
		List<LiquidRowElement> list = new ArrayList<LiquidRowElement>();
		Iterator<LiquidRowElement> rei = row_elements.iterator();
		while(rei.hasNext()){
			LiquidRowElement re = rei.next();
			if(re.col_content.equals(element)){
				list.add(re);
			}
		}
		row_elements.removeAll(list);
		updateTimestamp();
	}
	
	/**
	 * Remove all Elements
	 */
	public void clearElements(){
		row_elements.clear();
		updateTimestamp();
	}
	
	/**
	 * Liquid Row Element
	 */
	private static class LiquidRowElement{
		//@Expose
		//public int col_start;
		@Expose
		public int col_width;
		@Expose
		public HTMLElement col_content;
		
		public LiquidRowElement(/*int col_start,*/ int col_width, HTMLElement col_content){
			//this.col_start = col_start;
			this.col_width = col_width;
			this.col_content = col_content;
		}
	}
}
