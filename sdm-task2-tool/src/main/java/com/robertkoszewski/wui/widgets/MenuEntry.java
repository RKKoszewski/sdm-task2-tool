/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.wui.widgets;

import com.google.gson.annotations.Expose;
import com.robertkoszewski.wui.DynamicElement;
import com.robertkoszewski.wui.HTMLElement;
import com.robertkoszewski.wui.MenuElement;

/**
 * Menu Entry
 * @author Robert Koszewski
 */
public class MenuEntry extends DynamicElement implements HTMLElement, MenuElement{
	@Expose
	private String label;
	@Expose
	private String url;
	@Expose
	private String faicon;
	
	public MenuEntry(String label, String url, String faicon_id){
		this(label, url);
		this.faicon = faicon_id;
	}
	
	public MenuEntry(String label, String url){
		this.label = label;
		this.url = url;
	}
	
	public void setLabel(String label){
		this.label = label;
	}
	
	public String getLabel(){
		return this.label;
	}
	
	public void setURL(String url){
		this.url = url;
		updateTimestamp();
	}
	
	public String getURL(){
		return this.url;
	}
	
	public void setFAIcon(String icon_id){
		this.faicon = icon_id;
		updateTimestamp();
	}
}
