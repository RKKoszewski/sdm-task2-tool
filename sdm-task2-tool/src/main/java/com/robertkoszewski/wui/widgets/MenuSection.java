/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.wui.widgets;

import java.util.Vector;

import com.google.gson.annotations.Expose;
import com.robertkoszewski.wui.DynamicElement;
import com.robertkoszewski.wui.ExposeNode;
import com.robertkoszewski.wui.HTMLElement;
import com.robertkoszewski.wui.MenuElement;

/**
 * Menu Section
 * @author Robert Koszewski
 */
public class MenuSection extends DynamicElement implements HTMLElement, MenuElement{
	
	@ExposeNode
	private Vector<MenuEntry> menuentry = new Vector<MenuEntry>();
	
	@Expose
	private String title;
	
	public MenuSection(String title){
		this.title = title;
	}

	public void addMenuEntries(MenuEntry menu_entry) {
		this.menuentry.add(menu_entry);
	}
	
	// TODO: Finish this class
}
