/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.wui.widgets;

import java.util.Iterator;
import java.util.Vector;

import com.google.gson.annotations.Expose;
import com.robertkoszewski.wui.DynamicElement;
import com.robertkoszewski.wui.ExposeNode;
import com.robertkoszewski.wui.HTMLElement;
import com.robertkoszewski.wui.TimestampedElement;

/**
 * Panel
 * @author Robert Koszewski
 */
public class Panel extends DynamicElement implements HTMLElement{
	
	@Expose
	private String title;
	@ExposeNode
	private Vector<HTMLElement> content;
	
	public Panel(){}
	
	public Panel(String title){
		this.title = title;
		content = new Vector<HTMLElement>();
	}
	
	public Panel(String title, HTMLElement element){
		this(title);
		this.content.addElement(element);
	}
	
	public void setTitle(String title){
		this.title = title;
	}
	
	public String getTitle(){
		return this.title;
	}
	
	public void addElement(HTMLElement element){
		this.content.addElement(element);
	}
	
	public void removeElement(HTMLElement element){
		this.content.remove(element);
	}
	
	@Override
	public void updateTimestamp(){
		super.updateTimestamp();
		
		Iterator<HTMLElement> ci = content.iterator();
		while(ci.hasNext()){
			HTMLElement e = ci.next();
			if(e instanceof TimestampedElement){
				((TimestampedElement) e).updateTimestamp();
			}
		}
	}
	
	// TODO: Finish implementing this class
	
}
