/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.wui.widgets;

import java.util.Vector;

import com.google.gson.annotations.Expose;
import com.robertkoszewski.wui.DynamicElement;
import com.robertkoszewski.wui.ExposeNode;
import com.robertkoszewski.wui.HTMLElement;

/**
 * Table Class
 * @author Robert Koszewski
 */
public class Table extends DynamicElement implements HTMLElement{

	@Expose
	private String[] headers;
	@Expose
	private String[] column_widths;
	@ExposeNode
	private Vector<HTMLElement[]> rows;
	@Expose
	private String empty_text;
	
	public Table(){
		this.headers = null;
		this.rows = new Vector<HTMLElement[]>();
	}
	
	public Table(String... headers){
		this.headers = headers;
		this.rows = new Vector<HTMLElement[]>();
	}
	
	/**
	 * Set Table Column Widths
	 * @param column_width
	 */
	public void setColumnWidths(String... column_width){
		this.column_widths = column_width;
	}
	
	/**
	 * Add Row to Table
	 * @param row
	 */
	public void addRow(TableRow row){
		this.rows.addElement(row.getRows());
		updateTimestamp();
	}
	
	/**
	 * Add Row to Table Directly
	 * @param row_elements
	 */
	public void addRow(HTMLElement... row_elements){
		this.rows.addElement(row_elements);
		updateTimestamp();
	}
	
	/**
	 * Clear Rows
	 */
	public void cleanRows(){
		this.rows.clear();
		updateTimestamp();
	}
	
	/**
	 * Set Text that will be displayed when the table is empty
	 * @param empty_text
	 */
	public void setEmptyText(String empty_text){
		this.empty_text = empty_text;
		updateTimestamp();
	}
	
	/**
	 * Workaround for Table not Updating. TODO: Fix this
	 */
	@Deprecated
	public void forceUpdateTable(){
		updateTimestamp();
	}
}
