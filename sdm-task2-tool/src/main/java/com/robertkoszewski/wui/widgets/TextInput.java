/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package com.robertkoszewski.wui.widgets;

import com.google.gson.annotations.Expose;
import com.robertkoszewski.wui.DynamicElement;
import com.robertkoszewski.wui.HTMLElement;

/**
 * Text Input Element
 * @author Robert Koszewski
 */
public class TextInput extends DynamicElement implements HTMLElement{

	@Expose
	private String id;
	@Expose
	private String value;
	@Expose
	private Type type;
	@Expose
	private Boolean required;
	
	public TextInput(String id, String value, Type type){
		this.id = id;
		this.value = value;
		this.type = type;
	}
	
	public TextInput(String id, String value, Type type, boolean required){
		this.id = id;
		this.value = value;
		this.type = type;
		this.required = (required?true:null);
	}
	
	public void setValue(String value){
		this.value = value;
		updateTimestamp();
	}
	
	public String getID(){
		return this.id;
	}
	
	public enum Type{
		text,
		password,
		number,
		hidden
	}
}
