WUIEngine.registerController('Button', {
	// Set Data
	setData: function(dom, data){
		var button = $(dom);
		button.attr('type', data.type);
		button.attr('id', data.id);
		button.attr('value', data.label);
		if(data.type.toLowerCase() == 'submit'){
			button.attr('class', "btn btn-primary");
		}else{
			button.attr('class', "btn");
		}
		
		if(data.color != undefined && data.color.toLowerCase() != 'default'){
			button.addClass('btn-'+data.color.toLowerCase())
		}
	}
});