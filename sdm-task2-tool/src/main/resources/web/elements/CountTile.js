WUIEngine.registerController('CountTile', {
	// Set Data
	setData: function(dom, data){
		var jqcount_top = $(dom).children('.count_top');
		var count_top = jqcount_top[0];
		var jqcount_middle = $(dom).children('.count');
		
		// Text
		jqcount_top.text(data.title);
		jqcount_top.attr('title', data.title);
		jqcount_middle.text(data.data);
		
		// FaIcon
		if(data.faicon != undefined){
			var icon;
			if(!$(count_top.childNodes[0]).is('i')){
				icon = document.createElement('i');
				count_top.insertBefore(icon, count_top.childNodes[0]);
			}else{
				icon = node.childNodes[0];
			}
			var jqicon = $(icon);
			if(!jqicon.hasClass('fa') || !jqicon.hasClass('fa-marginr3') || !jqicon.hasClass('fa-'+data.faicon)){
				jqicon.removeClass().addClass('fa fa-marginr3 fa-'+data.faicon);
			}
			
		}else{
			if($(count_top.childNodes[0]).is('i')){
				count_top.removeChild(count_top.childNodes[0]);
			}
		}
	}
});