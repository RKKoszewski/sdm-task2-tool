WUIEngine.registerController('CountTileRow', {
	// Render Child Nodes
	renderChildNodes: function(dom, data){
		var i = 0;
		var cnodes = dom.childNodes;
		data = data.row_elements;
		
		if(data == undefined || data == null){
			//dom.parentNode.removeChild(dom);
			
		}else{	
			var wrapperTemplate = '<div></div>';
			var dataLocalizator = function(data){return data.col_content;}
			WUIEngine.template.renderWrappedNodes(dom, data, dataLocalizator, 'CountTileRow', wrapperTemplate, null, null);
			
		}
	}
});