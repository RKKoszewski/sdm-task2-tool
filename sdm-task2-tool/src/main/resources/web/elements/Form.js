WUIEngine.registerController('Form', {
	// Set Data
	setData: function(dom, data){
		$(dom).attr('id', data.id);
	},
	// Render Child Nodes
	renderChildNodes: function(dom, data){
		// Wrapper Template
		var wrapperTemplate = 				'<div class="form-group">';
		wrapperTemplate = wrapperTemplate + 	'<div></div>';
		wrapperTemplate = wrapperTemplate + '</div>';
		
		// Set force_vertical
		var force_vertical = data.force_vertical;
		// Wrapper Postprocess
		var wrapperPostprocess = function(node, data){
			jqnode = $(node);

			if(data.label != undefined && data.label != ''){
				
				console.log(force_vertical);
				
				var label
				if(!$(node.childNodes[0]).is('label')){
					label = document.createElement('label');
					if(force_vertical != undefined && force_vertical == true){
						$(label).addClass('control-label');
					}else{
						$(label).addClass('control-label col-md-3 col-sm-3 col-xs-12');
					}
					
					label.setAttribute('for',data.id);
					node.insertBefore(label, node.childNodes[0]);
				}else{
					label = node.childNodes[0];
				}
				
				var div = jqnode.children('div');
				if(force_vertical != undefined && force_vertical == true){
					div.removeClass();
				}else{
					if(!div.hasClass('col-md-9') || !div.hasClass('col-sm-9') || !div.hasClass('col-xs-12')){
						div.removeClass().addClass('col-md-9 col-sm-9 col-xs-12');
					}
				}

				if(label.innerText != data.label){
					label.innerText = data.label;
				}
				
				jqnode.children('div').children().first().attr('id', data.id);
				
			}else{
				var div = jqnode.children('div');
				if(force_vertical != undefined && force_vertical == true){
					div.removeClass();
				}else{
					if(!div.hasClass('col-md-12') || !div.hasClass('col-sm-12') || !div.hasClass('col-xs-12')){
						div.removeClass().addClass('col-md-12 col-sm-12 col-xs-12');
					}
				}
			}
		}
		// Node Localizator
		var wrapped_node_localizator = function(node){return $(node).children('div')[0];}
		// Data Localizator
		var dataLocalizator = function(data){return data.element;}
		
		WUIEngine.template.renderWrappedNodes(dom, data.form_elements, dataLocalizator, 'Form', wrapperTemplate, wrapped_node_localizator, wrapperPostprocess);

	},
	// Send Form Data
	sendForm: function(form){
		var data = $(form).serializeArray().reduce(function(obj, item) {
		    obj[item.name] = item.value;
		    return obj;
		}, {});
		WUIEngine.sendData(form.id, data, null, true);
	}
});