WUIEngine.registerController('Label', {
	// Set Data
	setData: function(dom, data){
		// Clear current nodes
		while (dom.hasChildNodes()) {
			dom.removeChild(dom.lastChild);
		}
		
		// Generate Label with or without Link
		if(data.href != undefined){
			var a = document.createElement("a");
			a.href = data.href;
			a.innerText = data.text;
			dom.appendChild(a);
		}else{
			dom.innerText = data.text;
		}
		
		if(data.faicon != undefined){
			var icon;
			if(!$(dom.childNodes[0]).is('i')){
				icon = document.createElement('i');
				dom.insertBefore(icon, node.childNodes[0]);
			}else{
				icon = node.childNodes[0];
			}
			
			var jqicon = $(icon);
			if(!jqicon.hasClass('fa') || !jqicon.hasClass('fa-inicon') || !jqicon.hasClass('fa-'+data.faicon)){
				jqicon.removeClass().addClass('fa fa-inicon fa-'+data.faicon);
			}
			
			if(data.faiconsize != undefined){
				jqicon.css('font-size', data.faiconsize+'px');
			}else{
				jqicon.css('font-size', 'inherit');
			}
			
		}else{
			if($(dom.childNodes[0]).is('i')){
				dom.removeChild(dom.childNodes[0]);
			}
		}

		// Set Position
		dom.setAttribute('style','display: '+data.position);
	}
});