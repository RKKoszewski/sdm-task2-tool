WUIEngine.registerController('LineChart', {
	// Set Data
	setData: function(dom, data){
		
		var dataset = [];
		$.each(data.dataset, function(index, value) {
			dataset[index] = value;
		});
		
		var canvas
		if(dom.childNodes[0] == undefined){
			canvas = document.createElement("canvas");
			dom.appendChild(canvas);
			
			 var startingData = {
		      labels: data.labels,
		      datasets: dataset
		    };

			 console.debug(startingData);
			 
		    dom.chart = new Chart(canvas , {
		        type: 'line',
		        labels: data.labels,
		        data: startingData,
		        options: {
		        	animation: false,
		            responsive: true
		        }
		    });
			
		}else{
			canvas = dom.childNodes[0];
			dom.chart.data.datasets = dataset;
			dom.chart.data.labels = data.labels;
			dom.chart.update();
			// Implement Sync/Push Updates with Animations?
			//dom.chart.data.datasets[0].data.push(5)
		}
		
	}
});