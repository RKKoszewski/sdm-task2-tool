WUIEngine.registerController('LiquidRow', {
	// Render Child Nodes
	renderChildNodes: function(dom, data){
		var i = 0;
		var cnodes = dom.childNodes;
		data = data.row_elements;
		
		if(data == undefined || data == null){
			//dom.parentNode.removeChild(dom);
			
		}else{	
			var wrapperTemplate = '<div></div>';
			var wrapperPostprocess = function(node, data){
				jqnode = $(node);
				if(!jqnode.hasClass("col-md-"+data.col_width) || !jqnode.hasClass("col-sm-"+data.col_width) || !jqnode.hasClass("col-xs-12")){
					jqnode.removeClass(); // Remove all Classes
					jqnode.addClass("col-md-"+data.col_width);
					jqnode.addClass("col-sm-"+data.col_width);
					jqnode.addClass("col-xs-12");
				}
			}
			var dataLocalizator = function(data){return data.col_content;}
			WUIEngine.template.renderWrappedNodes(dom, data, dataLocalizator, 'LiquidRow', wrapperTemplate, null, wrapperPostprocess);
			
		}
	}
});