WUIEngine.registerController('MenuEntry', {
	// Set Data
	setData: function(dom, data){
		var link = $(dom).children('a');
		link.attr('href', data.url);
		link.text(data.label);
		if(data.faicon != undefined){
			link.html('<i class="fa fa-'+data.faicon+'"></i> '+link.html());
		}
	}
});