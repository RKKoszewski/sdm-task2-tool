WUIEngine.registerController('MenuSection', {
	// Set Data
	setData: function(dom, data){
		$(dom).children("h3").text(data.title);
	},
	// Render Child Nodes
	renderChildNodes: function(dom, data){
		WUIEngine.template.renderNode($(dom).children("ul")[0], data.menuentry);
	}
});