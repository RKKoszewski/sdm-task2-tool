WUIEngine.registerController('Panel', {
	// Set Data
	setData: function(dom, data){
		$(dom).children('.x_title').children('h2').text(data.title);
	},
	// Render Child Nodes
	renderChildNodes: function(dom, data){
		var content = $(dom).children('.x_content')[0]
		WUIEngine.template.renderNode(content, data.content);
		//content.appendChildren(<div class="clearfix"></div>); TODO: Finish this.
	}
});