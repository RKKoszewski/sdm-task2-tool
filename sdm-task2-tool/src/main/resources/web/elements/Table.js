WUIEngine.registerController('Table', {
	// Set Data
	setData: function(dom, data){
		var table = $(dom);

		// Clear current nodes
		while (dom.hasChildNodes()) {
			dom.removeChild(dom.lastChild);
		}
		
		// Create Table Head
		var thead = document.createElement('thead');
		dom.appendChild(thead);
		var thead_tr = document.createElement('tr');
		thead.appendChild(thead_tr);
		var head_count = 0;
		
		// Fix Table Width
		if(data.column_widths == undefined){
			data.column_widths = [];
		}

		$.each(data.headers, function(index, header) {
			var thead_tr_th = document.createElement('th');
			thead_tr_th.appendChild(document.createTextNode(header));
			thead_tr.appendChild(thead_tr_th);
			if(data.column_widths[index] != undefined){
				thead_tr_th.setAttribute("width", data.column_widths[index]);
			}
			head_count = head_count +1;
		});
		
		// Create Table Rows
		var tbody = document.createElement('tbody');
		dom.appendChild(tbody);
		var row_count = 0;
		
		$.each(data.rows, function(index, row) {
			var tbody_tr = document.createElement('tr');
			tbody.appendChild(tbody_tr);
			row_count = row_count +1;

			$.each(row, function(index, element) {
				var tbody_tr_td = document.createElement('td');
				tbody_tr.appendChild(tbody_tr_td);
				WUIEngine.template.renderNode(tbody_tr_td, [element]);
				console.debug(element);
			});
		});
		
		// Show Empty Message
		if(row_count == 0 && data.empty_text != undefined){
			var tbody_tr = document.createElement('tr');
			tbody.appendChild(tbody_tr);
			var tbody_tr_td = document.createElement('td');
			tbody_tr.appendChild(tbody_tr_td);
			tbody_tr_td.colSpan = head_count;
			tbody_tr_td.appendChild(document.createTextNode(data.empty_text));
			tbody_tr_td.setAttribute("align", "center");
		}
	}
});