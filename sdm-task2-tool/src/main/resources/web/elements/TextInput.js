WUIEngine.registerController('TextInput', {
	// Set Data
	setData: function(dom, data){
		var button = $(dom);
		if(data.type == 'hidden'){
			button.attr('type', 'text');
			button.css('display','none');
		}else{
			button.attr('type', data.type);
			button.css('display','inherit');
		}
		button.attr('name', data.id);
		if(data.value != undefined){
			dom.value = data.value;
		}
	}
});