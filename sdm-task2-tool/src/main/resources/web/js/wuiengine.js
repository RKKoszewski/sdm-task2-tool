/*******************************************************************************
 * Copyright (c) 2016 Robert Koszewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/

/**
 * WUI Engine 1.0
 * Programmed by Robert Koszewski
 */

var WUIEngine = [];
WUIEngine.template = [];
WUIEngine.template.elements = [];
WUIEngine.constants = [];
WUIEngine.utils = [];

(function () {
	
	// Static Variables
	var basePath = '/wui';
	var wuiElementId = 'wui.element';
	var wuiElementUID = 'wui.uid';
	var wuiElementNoChange = 'wui.nochange';
	
	// Externalise Constants
	WUIEngine.constants.elementType = wuiElementId;
	WUIEngine.constants.elementGUID = wuiElementUID;
	WUIEngine.constants.wuiElementNoChange = wuiElementNoChange;
	
	// Caching Variables
	var baseTemplate = null;
	var elementTemplate = [];
	WUIEngine.template.elements = elementTemplate;

	// Required Variables
	var elementControllers = [];
	var last_timestamp = 0;
	
	// Element Variables
	var bodyNode;
	
	// Initialize WUI Engine
	window.onload = function(){
		// Get Initial Components
		bodyNode = $("body");
		
		// Bind HistoryJS
		History.Adapter.bind(window,'statechange',function(){
			//var State = History.getState();
			last_timestamp = 0;
			getPage(window.location.href, null, true);
		});

		// Get Base Template
		getElementTemplates(function(){
			getPage(window.location.href, null, true);
			setInterval(function(){
				getPage(window.location.href);
			} , 1000);
		});
	}

	// Get all Element Templates
	var getElementTemplates = function(callback){
		$.getJSON(basePath + '/elements' , function(data) {
			  $.each(data, function(element, source) {
				  elementTemplate[element] = getDOMElementFromString(source); // Compile Element (RAW Javascript)
				  elementTemplate[element].setAttribute(wuiElementId, element);
				  // elementTemplate[element] = Handlebars.compile(source); // Compile Element (Handlebars)
				  console.log("Registering element: "+element);
			  });
			  exec(callback);
		});
	}
	
	// Get DOM Element from String
	var getDOMElementFromString = function(src){
		var node = document.createElement("template");
		node.innerHTML = src;
		return node.content.firstChild;
	}
	
	WUIEngine.utils.getDOMElementFromString = getDOMElementFromString;
	
	// Get Page
	var getPage = function(url, data, show_progress){
		// Show Progress
		if(show_progress==true){
			NProgress.start();
			WUIEngine.template.setBusyState(true);
		}
		
		// Reset Elements
		if(last_timestamp == 0){
			WUIEngine.template.resetElements();
		}
		
		// Get Page Content
		getJSON(url+'?wui-timestamp='+last_timestamp , data, function(data, textStatus, jqXHR) {
			
			// No-Operation
			if(data == null || data.wui == undefined){
				console.log("No-Op")
				if(show_progress==true){NProgress.done();}
				return;
			}
			
			if(show_progress==true){NProgress.set(0.4)}
			
			// Render Content
			WUIEngine.template.render(data);
			
			if(show_progress==true){NProgress.set(0.8);}
			
			// Set Timestamp
			last_timestamp = data.timestamp;
			
			if(show_progress==true){
				NProgress.done();
				WUIEngine.template.setBusyState(false);
			}
			
			// Rebind Click Event
			$('a').bind('click', function( event ){
			    event.preventDefault();
			    var href = $(this).attr('href');
			    History.pushState(null, null, href);
			});
			
			// Redirects
			if(data.wui_redirect){
				History.pushState(null, null, data.wui_redirect);
			}

		});
	}
	
	// Render Element
	var renderElement = function(data){
		var html = "";
		if(data[wuiElementId]){
			// Render WUI Element
			// @DEBUG: console.log("Rendering: "+data[wuiElementId]);
			var etempl = elementTemplate[data[wuiElementId]];
			if(etempl == undefined){
				if(data[wuiElementUID] == undefined){
					return '<span id="'+data[wuiElementUID]+'"><span>';
				}
				console.error("ERROR: Element template for '"+data[wuiElementId]+"' doesn't exist and cannot be rendered.")
			}else{
				if(data[wuiElementUID] == undefined){
					return elementTemplate[data[wuiElementId]](data);
				}else{
					return '<span id="'+data[wuiElementUID]+'">'+elementTemplate[data[wuiElementId]](data)+'<span>';
				}
			}

		}else if(data instanceof Array){
			// Render Array of Elements
			data.forEach(function(entry){
				html = html + renderElement(entry);
			});
			
		}else{
			alert("ERROR: Element is not a WUI Element");
		}
		
		return html;
	}
	
	var syncContent = function(){
		
	}
	
	// Execute Callback
	var exec = function(callback, param1, param2, param3, param4){
		if(callback != null && callback != undefined){
			callback(param1, param2, param3, param4);
		}
	}
	
	// Send Data to Server
	var sendData = function(request_id, data, url, showbusy){
		if(showbusy == true){
			WUIEngine.template.setBusyState(true);
		}
		
		if(url == undefined || url == null){
			url = window.location.href;
		}
		data['wui.dataid'] = request_id;
		return getPage(url, data, showbusy);
	}
	
	// Communication Functions
	var getJSON = function(url, data, success_callback, failure_callback){
		$.ajax({
			  url: url,
			  headers: {'WUIRequestAction':'buttonClicked'}, // WUI Request Types could be: {content}, data, raw
			  dataType: 'json',
			  method: 'POST',
			  data: data,
			  success: success_callback,
			  failure: failure_callback
		});
	}
	
	// Register Controllers
	var registerController = function(element, controller){
		console.log("Registering controller for element: "+element);
		elementControllers[element] = controller;
	}
	
	// Global WUIEngine Functions
	WUIEngine.registerController = registerController;
	WUIEngine.Control = elementControllers;
	WUIEngine.controller = elementControllers;
	WUIEngine.reloadPage = function(){getPage(window.location.href);};
	WUIEngine.sendData = sendData;
	WUIEngine.t = elementTemplate;

})();