/**
 * Gentelella Template Engine for WUI Engine
 * Programmed by Robert Koszewski
 */

(function () {

	// Initialize Data and Pointers
	var content;
	var menu;
	var navtitle;
	
	// Static Variables
	var basePath = '/wui';
	var wuiElementId = 'wui.element';
	var wuiElementUID = 'wui.uid';
	
	// DOM Elements
	var elements = []; // elements.{GUID}.
	
	// Run when Document is Ready
	$(function() {
		 menu = $('#sidebar-menu')[0];
		 content = $('#content')[0];
		 navtitle = $('#navbar-title')[0];
	});
	
	// Setup WUI Environment
	//WUIEngine.template = [];
	WUIEngine.template.name = 'Gentelella';

	// Register UI renderer to WUI Engine
	WUIEngine.template.render = function(data){
		
		// Set Page Title
		if(data.title){
			document.title = data.app_name + ' - ' + data.title;
			navtitle.innerText = data.title;
		}

		// Notifications
		if(data.wui_notifications != undefined){
			$.each(data.wui_notifications, function(index, notification) {
				new PNotify({
                    title: notification.title,
                    text: notification.message,
                    type: notification.type.toLowerCase(),
                    styling: 'bootstrap3'
                });
			});
		}
		
		// Render Menu
		renderNode(menu, data.menu);
		
		// Render Content
		renderNode(content, data.body);
	};
	
	// Reset Elements
	var resetElements = function(){
		elements = [];
	}
	
	// Make function publicly available
	WUIEngine.template.resetElements = resetElements;
	
	// Renders a Node
	var renderNode = function(dom, data){
		var i = 0;
		var cnodes = dom.childNodes;
		if(data == undefined || data == null){
			dom.parentNode.removeChild(dom);
			
		}else{	

			if(data.length == undefined){
				data = [data];
			}
			
			while(data.length>i){
				if(cnodes == undefined || cnodes[i] == undefined){
					// Create New
					node = getNode(data[i]);
					dom.appendChild(node);
					updateNodeData(node, data[i]);
					
				}else{
					// Compare
					if(cnodes[i][wuiElementUID] == undefined || cnodes[i][wuiElementUID] != data[i][wuiElementUID]){
						// Replace Node
						node = getNode(data[i]);
						dom.replaceChild(node, cnodes[i]);
						updateNodeData(node, data[i]);
						
					}
				}
				i = i + 1;
			}
			// Remove all unnecessary nodes left
			while(cnodes.length > data.length){
				dom.removeChild(cnodes[data.length]);
				i = i + 1;
			}
		}
	};
	
	// Make function publicly available
	WUIEngine.template.renderNode = renderNode;
	
	// Render Wrapped Node
	var renderWrappedNodes = function(dom, data, data_localizator, wrapped_id_prefix, wrapper_dom, wrapped_node_localizator, wrapper_postprocess, index, is_last_node){
		// Without Index
		if(index == null){
			var i = 0;
			while(data.length > i){
				renderWrappedNodes(dom, data[i], data_localizator, wrapped_id_prefix, wrapper_dom, wrapped_node_localizator, wrapper_postprocess, i, data.length-1 == i);
				i = i + 1;
			}
			
			if(data.length == 0){
				// Just Remove Content
				while(dom.childNodes.length != 0){
					dom.removeChild(dom.childNodes[0]);
				}
			}
			
		}else{

			// Localize Data
			var localized_data;
			if(data_localizator == null){
				localized_data = data;
			}else{
				localized_data = data_localizator(data);
			}
			
			// Get First Element UID
			var ueid;
			if(localized_data.length != undefined){
				ueid = localized_data[0][WUIEngine.constants.elementGUID];
			}else{
				ueid = localized_data[WUIEngine.constants.elementGUID];
			}

			// Get Custom Node
			var node = WUIEngine.template.getCustomNode(wrapped_id_prefix+'wrp'+ ueid, function(){
				// Check if Wrapped node is a String
				if(typeof wrapper_dom === 'string' || wrapper_dom instanceof String){
					return WUIEngine.utils.getDOMElementFromString(wrapper_dom);
				}else{
					return wrapper_dom;
				}
			});
			
			// Wrapper Node Post-Process
			if(wrapper_postprocess != undefined || wrapper_postprocess != null){
				wrapper_postprocess(node, data, localized_data);
			}

			var cnodes = dom.childNodes;
			
			// Deal with Child Nodes
			if(cnodes == undefined || cnodes[index] == undefined){
				dom.appendChild(node);
				
			}else{
				// Compare
				if(cnodes[index][WUIEngine.constants.elementGUID] == undefined || cnodes[index][WUIEngine.constants.elementGUID] != localized_data[WUIEngine.constants.elementGUID]){
					dom.replaceChild(node, cnodes[index]);
				}
			}
			
			// Render Child Nodes
			var wrapped_node;
			if(wrapped_node_localizator == null){
				wrapped_node = node;
			}else{
				wrapped_node = wrapped_node_localizator(node)
			}

			// Render Node
			renderNode(wrapped_node, localized_data);
			
			// Remove all unnecessary nodes left
			if(is_last_node){
				while(cnodes.length > index+1){
					dom.removeChild(cnodes[cnodes.length-1]);
				}
			}
		}
	}
	
	// Make function publicly available
	WUIEngine.template.renderWrappedNodes = renderWrappedNodes;
	
	// Returns a Node
	var getNode = function(data){
		if(elements[data[wuiElementUID]] == undefined){ 
			// Initialize Element if Not Exist	
			var template = WUIEngine.template.elements[data[wuiElementId]];
			if(template == undefined){
				// Show error if Template not defined
				elements[data[wuiElementUID]] = document.createElement('span'); // Create Dummy Element
				elements[data[wuiElementUID]].setAttribute(wuiElementUID, data[wuiElementUID]); // TODO: Check for errors
				console.error("ERROR: Element template for '"+data[wuiElementId]+"' doesn't exist and cannot be rendered.");
				
			}else{
				// Initialize Node
				elements[data[wuiElementUID]] = template.cloneNode(true);
				elements[data[wuiElementUID]].setAttribute(wuiElementUID, data[wuiElementUID]); // TODO: Check for errors
				
			}
		}
		
		return elements[data[wuiElementUID]];
	};
	
	// Make function publicly available
	WUIEngine.template.getNode = getNode;
	
	// Get a Custom Node
	var getCustomNode = function(uid, factory){
		if(elements[uid] == undefined){ 
			// Initialize Element if Not Exist
			elements[uid] = factory();
			elements[uid].setAttribute(wuiElementUID, uid);
		}
		
		return elements[uid];
	};
	
	// Make function publicly available
	WUIEngine.template.getCustomNode = getCustomNode;
	
	// Update Node Data
	var updateNodeData = function(node, data){
		var controller = WUIEngine.controller[data[wuiElementId]];
		if(controller != undefined){
			// Update Data
			if(controller.setData != undefined){
				if(data[WUIEngine.constants.wuiElementNoChange] == undefined){
					controller.setData(node, data);
				}
			}
			// Update Child Nodes
			if(controller.renderChildNodes != undefined){
				controller.renderChildNodes(node, data);
			}
		}
	}
	
	// Set Busy State
	var setBusyState = function(isBusy){
		if(isBusy){
			$(document.body).addClass('busy');
		}else{
			$(document.body).removeClass('busy');
		}
	}
	
	// Make function publicly available
	WUIEngine.template.setBusyState = setBusyState;
	
})();